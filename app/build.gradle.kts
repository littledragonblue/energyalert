@file:Suppress("SpellCheckingInspection")
import java.text.SimpleDateFormat
import java.io.FileInputStream
import java.util.*

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

fun getCurrentDate(): Int {
    val dateFormat = SimpleDateFormat("yyMMdd", Locale.getDefault())
    return dateFormat.format(Date()).toInt()
}

val secretsProperties = Properties()
val secretsPropertiesFile: File = rootProject.file("secrets.properties")
if (secretsPropertiesFile.exists()) {
    secretsProperties.load(FileInputStream(secretsPropertiesFile))
}

android {
    namespace = "eu.samoreira.energyalert"
    compileSdk = 34

    defaultConfig {
        applicationId = "eu.samoreira.energyalert"
        targetSdk = 34
        versionCode = getCurrentDate()
        multiDexEnabled = true
        testFunctionalTest = true
        testHandleProfiling = true

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        manifestPlaceholders["backupApiKey"] = secretsProperties["BACKUP_API_KEY"] ?: ""
        buildConfigField("String", "APP_KEY", "\"${secretsProperties["APP_KEY"].toString()}\"")
        buildConfigField("String", "SERVER_URL", "\"${secretsProperties["SERVER_URL"].toString()}\"")
        buildConfigField("String", "EMAIL_URL", "\"${secretsProperties["EMAIL_URL"].toString()}\"")
        buildConfigField("String", "SERVER_URL_HTTP", "\"${secretsProperties["SERVER_URL_HTTP"].toString()}\"")
        buildConfigField("String", "SERVER_URL_TEST", "\"${secretsProperties["SERVER_URL_TEST"].toString()}\"")
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    flavorDimensions += "version"
    productFlavors {
        create("energyalert") {
            dimension = "version"
            setProperty("archivesBaseName", "app")
            minSdk = 21
            versionCode = getCurrentDate()
            targetSdk = 34
            testFunctionalTest = true
            testHandleProfiling = true
            versionName = "1.$versionCode.$minSdk$targetSdk"

            // Comentar para compilar legacy version
            dependencies {
            //    implementation("androidx.core:core-ktx:1.13.1")
            //    implementation("com.google.android.material:material:1.12.0")
            //    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.8.2")
            //    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.8.2")
            //    implementation("androidx.appcompat:appcompat:1.7.0")
            //    implementation("androidx.vectordrawable:vectordrawable:1.2.0")
            }
        }

        create("energyalert_legacy") {
            dimension = "version"
            setProperty("archivesBaseName", "app")
            minSdk = 16
            versionCode = getCurrentDate()
            targetSdk = 34
            testFunctionalTest = true
            testHandleProfiling = true
            versionName = "1.$versionCode.$minSdk$targetSdk"

            dependencies {
                //noinspection GradleDependency
                implementation("androidx.core:core-ktx:1.12.0")
                //noinspection GradleDependency
                implementation("androidx.appcompat:appcompat:1.6.1")
                //noinspection GradleDependency
                implementation("androidx.vectordrawable:vectordrawable:1.1.0")
                //noinspection GradleDependency
                implementation("com.google.android.material:material:1.11.0")
                //noinspection GradleDependency
                implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.6.1")
                //noinspection GradleDependency
                implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.6.1")
            }
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    buildFeatures {
        viewBinding = true
        buildConfig = true
    }

    bundle {
        storeArchive {
            enable = true
        }
    }

    lint {
        baseline = file("lint-baseline.xml")
    }
}

dependencies {
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.navigation:navigation-fragment-ktx:2.7.7")
    implementation("androidx.navigation:navigation-ui-ktx:2.7.7")
    implementation("androidx.multidex:multidex:2.0.1")
    implementation("androidx.recyclerview:recyclerview:1.3.2")
    implementation("com.android.volley:volley:1.2.1")
    implementation("androidx.work:work-runtime-ktx:2.9.0")

    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.2.1")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.6.1")
}