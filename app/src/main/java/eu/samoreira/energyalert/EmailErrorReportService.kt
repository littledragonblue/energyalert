package eu.samoreira.energyalert

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import eu.samoreira.energyalert.database.DatabaseHandler
import eu.samoreira.energyalert.utils.AppUtils
import eu.samoreira.energyalert.utils.Constants

class EmailErrorReportService: Service() {

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    fun sendEmail(context: Context, message: String, function: String) {
        val databaseHandler = DatabaseHandler(context)
        val emailMessage    = context.getString(R.string.app_name) + "(Error Report): " + message +
            "<p>Function: " + function + "</p>"

        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(context)
        val url   = AddServer.getServerUrl(context) + Constants.EMAIL_SERVICE_URL + "&email=1"

        if (BuildConfig.DEBUG) {
            println(url)
            println(emailMessage)
        }

        // Request a string response from the provided URL.
        val stringRequest: StringRequest = object: StringRequest(Method.POST, url, Response.Listener { response ->
            AppUtils.toast(context, "Error report: $response")
        }, Response.ErrorListener {
            AppUtils.toast(context, "Error! Sending report error!")
        }) {
            override fun getParams(): Map<String, String> {
                val postParam: MutableMap<String, String> = java.util.HashMap()
                if (databaseHandler.viewEmailSettings()[0].email.isNotEmpty()) {
                    postParam["host"] = databaseHandler.viewEmailSettings()[0].host
                    postParam["username"] = databaseHandler.viewEmailSettings()[0].email
                    postParam["password"] = databaseHandler.viewEmailSettings()[0].password
                    postParam["SMTPSecure"] = databaseHandler.viewEmailSettings()[0].smtp
                    postParam["port"] = databaseHandler.viewEmailSettings()[0].port.toString()
                }
                postParam["message"] = emailMessage
                return postParam
            }
        }

        databaseHandler.close()
        queue.add(stringRequest)
    }
}