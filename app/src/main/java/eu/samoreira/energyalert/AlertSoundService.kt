package eu.samoreira.energyalert

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.IBinder
import android.os.VibrationEffect
import android.os.Vibrator
import android.os.VibratorManager
import androidx.core.app.NotificationCompat
import eu.samoreira.energyalert.utils.Constants
import kotlin.system.exitProcess

class AlertSoundService: Service() {
    private var mediaPlayer: MediaPlayer? = null

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            ACTION_START -> {
                start()
                startVibration()
            }
            ACTION_STOP -> stopService()
            ACTION_FINISH_APP -> {
                stopService()
                exitProcess(0)
            }
        }
        return super.onStartCommand(intent, flags, startId)
    }

    private fun start() {
        val notification = buildForegroundNotification()
        startForeground(Constants.NOTIFICATION_ID, notification)
    }


    private fun buildForegroundNotification(title: String = getString(R.string.app_name)): Notification {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { // API 23
            val finishPomodoroIntent = Intent(this, AlertSoundService::class.java).apply {
                action = ACTION_FINISH_APP
            }
            val finishPomodoroPendingIntent = PendingIntent.getService(this, Constants.PERMISSION_REQUEST_CODE, finishPomodoroIntent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)
            return NotificationCompat.Builder(this, Constants.CHANNEL_ALARM_ID)
                .setContentTitle(title)
                .setContentText(getString(R.string.sound_alert_activated))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setColorized(true)
                .setCategory(ACTION_START)
                .setSmallIcon(R.drawable.logo_24)
                .setOngoing(true)
                .addAction(R.drawable.ic_exit, getString(R.string.exit), finishPomodoroPendingIntent)
                .build()
        } else {
            return NotificationCompat.Builder(this, Constants.CHANNEL_ALARM_ID)
                .setContentTitle(title)
                .setContentText(getString(R.string.sound_alert_activated))
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSound(null)
                .setSmallIcon(R.drawable.logo_24)
                .setOngoing(true)
                .build()
        }
    }


    private fun stopService() {
        stopForeground(true)
        stopSelf()

        val stopActionStartedIntent = Intent(ACTION_STOP)
        sendBroadcast(stopActionStartedIntent)
        Constants.isSoundActive = false

        mediaPlayer?.release()
        mediaPlayer = null
    }

    private fun startVibration() {
        val vib = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            val vibratorManager = getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager
            vibratorManager.defaultVibrator
        } else {
            @Suppress("DEPRECATION")
            getSystemService(VIBRATOR_SERVICE) as Vibrator
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vib.vibrate(VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE) )
        }else{
            @Suppress("DEPRECATION")
            vib.vibrate(1000)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Constants.isSoundActive = false
        stopService()
    }

    companion object {
        const val ACTION_START = "ACTION_START"
        const val ACTION_STOP = "ACTION_STOP"
        const val ACTION_FINISH_APP = "ACTION_FINISH_APP"
    }
}