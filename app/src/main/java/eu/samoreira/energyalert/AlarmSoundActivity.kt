package eu.samoreira.energyalert

import android.app.Activity
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView

class AlarmSoundActivity: Activity() {
    private val notification: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
    private lateinit var alarmSound: Ringtone

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
            (WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
        )

        setContentView(R.layout.activity_alarm_sound)

        val message = intent.extras!!.getString("message")
        alarmSound = RingtoneManager.getRingtone(this@AlarmSoundActivity, notification)
        val alarmMessage = findViewById<TextView>(R.id.alarmMessage)
        val button = findViewById<Button>(R.id.button)

        alarmMessage.text = message

        button.setOnClickListener {
            alarmSound.stop()
            finish()
        }

        alarmSound.play()
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_BACK, KeyEvent.KEYCODE_ESCAPE, KeyEvent.KEYCODE_BUTTON_B, KeyEvent.KEYCODE_HOME -> {
                alarmSound.stop()
                finish()
                return true
            }
        }

        return super.onKeyUp(keyCode, event)
    }

    override fun onDestroy() {
        alarmSound.stop()
        finish()
        super.onDestroy()
    }
}