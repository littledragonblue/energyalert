package eu.samoreira.energyalert

import android.app.backup.BackupAgentHelper
import android.app.backup.FileBackupHelper
import android.app.backup.SharedPreferencesBackupHelper
import eu.samoreira.energyalert.utils.Constants

class BackupAgent: BackupAgentHelper() {

    override fun onCreate() {
        val helperSharedPreferences = SharedPreferencesBackupHelper(this, Constants.SHARED_PREFERENCES_KEY)
        addHelper(Constants.SHARED_PREFERENCES_KEY, helperSharedPreferences)

        val helperSqlite = FileBackupHelper(this, Constants.SQLITE_KEY)
        addHelper(Constants.SQLITE_KEY, helperSqlite)
    }
}