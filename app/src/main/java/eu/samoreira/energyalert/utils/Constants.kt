package eu.samoreira.energyalert.utils

import eu.samoreira.energyalert.BuildConfig

interface Constants {
    companion object {

        const val AUTHOR = "Sérgio Moreira"
        const val DIANE = "Diane Braeckmans"
        const val MENDANHA = "Vitor Mendanha"
        const val APP_VERSION = BuildConfig.VERSION_NAME
        const val APPLICATION_ID = BuildConfig.APPLICATION_ID
        const val SHARED_PREFERENCES_KEY = "app_$APPLICATION_ID"
        const val SQLITE_KEY = "sqlite_$APPLICATION_ID"
        const val SERVER_URL = BuildConfig.SERVER_URL
        const val TEST_SERVER_URL = BuildConfig.SERVER_URL_TEST
        const val SERVER_HTTP_URL = BuildConfig.SERVER_URL_HTTP
        const val APP_DOWNLOAD_URL = "https://samoreira.eu/?download=energyalert"
        const val EMAIL_SERVICE_URL = BuildConfig.EMAIL_URL
        const val PERMISSION_REQUEST_CODE: Int = 7
        const val SMS_PERMISSION_REQUEST_CODE: Int = 9

        const val CHANNEL_ALARM_ID = "ALARM_NOTIFICATIONS"
        const val CHANNEL_SERVICE_ID = "SERVICE_NOTIFICATIONS"
        const val NOTIFICATION_ID: Int = 7
        const val CHANNEL_SERVICE_NAME = "Services Notifications"
        const val CHANNEL_ALARM_NAME = "Alarms Notifications"
        const val CHANNEL_DESCRIPTION = "Notification for running services of the app"

        var isPhoneActive: Boolean = false
        var isEmailActive: Boolean = false
        var isSoundActive: Boolean = false
    }
}