package eu.samoreira.energyalert.utils

import android.content.Context
import android.util.Log
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import eu.samoreira.energyalert.AddServer
import eu.samoreira.energyalert.BuildConfig

class ErrorReport(private val context: Context) {

    private val TAG = "TAG ErrorReport"
    private val queue = Volley.newRequestQueue(context)

    fun reportError(email: String, title: String, message: String, onSuccess: (String) -> Unit, onError: (String) -> Unit) {
        val url = AddServer.getServerUrl(context) + "/services/bugreport.php?app=" + Constants.APPLICATION_ID

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "url: $url")
            Log.d(TAG, "subtitle: $title")
            Log.d(TAG, "message: $message")
        }

        val stringRequest: StringRequest = object: StringRequest(Method.POST, url, Response.Listener { response ->
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Report: $response")
            }
            onSuccess(response)
        }, Response.ErrorListener { error ->
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "Error! Sending report error! $error")
            }
            onError(error.toString())
        }) {
            override fun getParams(): Map<String, String> {
                val postParam: MutableMap<String, String> = java.util.HashMap()
                postParam["appversion"] = Constants.APP_VERSION
                postParam["isandroid"] = "yes"
                postParam["email"] = email
                postParam["title"] = title
                postParam["message"] = message
                return postParam
            }
        }

        queue.add(stringRequest)
    }
}