package eu.samoreira.energyalert.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityOptions
import android.app.NotificationManager
import androidx.appcompat.app.AlertDialog
import android.app.UiModeManager
import android.content.ComponentName
import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.content.Context.UI_MODE_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.*
import android.webkit.WebView
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import eu.samoreira.energyalert.BackupAgent
import eu.samoreira.energyalert.BatteryMonitoringService
import eu.samoreira.energyalert.BootCompletedReceiver
import eu.samoreira.energyalert.BuildConfig
import eu.samoreira.energyalert.database.DatabaseHandler
import eu.samoreira.energyalert.database.DrawMessageClass
import eu.samoreira.energyalert.EmailService
import eu.samoreira.energyalert.OpenInternetSettings
import eu.samoreira.energyalert.R
import eu.samoreira.energyalert.ReceiverManager
import eu.samoreira.energyalert.SmsService
import eu.samoreira.energyalert.SoundService
import java.util.*
import kotlin.system.exitProcess

object AppUtils {
    private const val TAG = "TAG AppUtils"

    @JvmStatic
    fun getSharedPreferencesBoolean(context: Context?, key: String?): Boolean {
        val sharedPreferences = context?.getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, 0)
        return sharedPreferences!!.getBoolean(key, false)
    }

    fun setSharedPreferencesBoolean(context: Context?, key: String, boolean: Boolean) {
        val sharedPreferences = context?.applicationContext?.getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, 0)
        val editor = sharedPreferences?.edit()
        editor?.putBoolean(key, boolean)
        editor?.apply()
    }

    fun getSharedPreferencesString(context: Context?, key: String?, string: String = ""): String? {
        val sharedPreferences = context?.getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, 0)
        return sharedPreferences?.getString(key, string)
    }

    fun setSharedPreferencesString(context: Context?, key: String, string: String) {
        val sharedPreferences = context?.applicationContext?.getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, 0)
        val editor = sharedPreferences?.edit()
        editor?.putString(key, string)
        editor?.apply()
    }

    fun getSharedPreferencesInt(context: Context, key: String, value: Int = 0): Int {
        val sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, 0)
        return sharedPreferences.getInt(key, value)
    }

    fun setSharedPreferencesInt(context: Context, key: String, value: Int = 0) {
        val sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, 0)
        val editor = sharedPreferences?.edit()
        editor?.putInt(key, value)
        editor?.apply()
    }

    private fun checkPreferencesExists(context: Context, key: String?): Boolean {
        val sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, 0)
        return sharedPreferences.contains(key)
    }

	@JvmStatic
	fun toast(context: Context?, message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    fun getConnection(context: Context): Boolean {
        val cm = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)
            capabilities != null
        } else {
            val activeNetwork = cm.activeNetworkInfo
            activeNetwork != null
        }
    }

    @JvmStatic
    fun closeApp(context: Context?) {
        AlertDialog.Builder(context!!).apply {
            setTitle(context.getString(R.string.do_you_want_exit_app))
            setCancelable(false)
            setIcon(R.drawable.ic_exit)

            val inflater = LayoutInflater.from(context)
            val closeView: View = inflater.inflate(R.layout.layout_exit, null)
            setView(closeView)

            val btnYES = closeView.findViewById<Button>(R.id.yes)
            val btnNO  = closeView.findViewById<Button>(R.id.no)

            val exitDialog = create()
            exitDialog.window?.setSoftInputMode(1)

            btnYES.setOnClickListener {
                // 1. Stop services
                // For multiple services, repeat the above two lines for each service
                val batteryServiceIntent = Intent(context, BatteryMonitoringService::class.java)
                context.stopService(batteryServiceIntent)

                val smsServiceIntent = Intent(context, SmsService::class.java)
                context.stopService(smsServiceIntent)

                val soundServiceIntent = Intent(context, SoundService::class.java)
                context.stopService(soundServiceIntent)

                val emailServiceIntent = Intent(context, EmailService::class.java)
                context.stopService(emailServiceIntent)

                val backupServiceIntent = Intent(context, BackupAgent::class.java)
                context.stopService(backupServiceIntent)

                // 2. Unregister BroadcastReceivers if registered dynamically
                val receiverComponent = ComponentName(context, BootCompletedReceiver::class.java)
                context.packageManager.setComponentEnabledSetting(
                    receiverComponent,
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP
                )

                try {
                    ReceiverManager.unregisterAll(context)
                } catch (e: Exception) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace()
                    }
                }

                // 3. Cancel notifications
                val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.cancelAll()

                // Finally, close the app
                exitProcess(0)
            }

            btnNO.setOnClickListener {
                exitDialog.dismiss()
            }

            if (exitDialog.window != null) {
                exitDialog.window!!.attributes.windowAnimations = R.style.SlidingDialogAnimation
            }
            exitDialog.show()
        }
    }

    @JvmStatic
    fun isAndroidTV(context: Context): Boolean  {
        val uiModeManager = context.getSystemService(UI_MODE_SERVICE) as UiModeManager
        return uiModeManager.currentModeType == Configuration.UI_MODE_TYPE_TELEVISION
    }

    @JvmStatic
    fun openAboutLayout(context: Context?) {
        val aboutDialog = AlertDialog.Builder(context!!)
        aboutDialog.setTitle(context.getString(R.string.about))
        aboutDialog.setCancelable(true)
        aboutDialog.setIcon(R.drawable.ic_info)

        val inflater = LayoutInflater.from(context)
        val aboutView: View = inflater.inflate(R.layout.layout_about, null)
        aboutDialog.setView(aboutView)

        val textAutor       = aboutView.findViewById<TextView>(R.id.text_author)
        val textAppVersion  = aboutView.findViewById<TextView>(R.id.txt_app_version)
        val textUrl         = aboutView.findViewById<TextView>(R.id.txt_url)
        val textTranslation = aboutView.findViewById<TextView>(R.id.txt_translation)
        val textDiane       = aboutView.findViewById<TextView>(R.id.txt_diane)
        val textMendanha    = aboutView.findViewById<TextView>(R.id.txt_mendanha)
        val btnAppGuide     = aboutView.findViewById<Button>(R.id.app_troubleshoot)
        val appInfo         = aboutView.findViewById<Button>(R.id.appInfo)
        val btnWebsite      = aboutView.findViewById<Button>(R.id.appDownloadUrl)
        val btnReportError  = aboutView.findViewById<Button>(R.id.reportError)
        val btnShare        = aboutView.findViewById<Button>(R.id.share)
        val btnExit         = aboutView.findViewById<Button>(R.id.exit)

        val dialogAbout = aboutDialog.create()
        dialogAbout.window!!.setSoftInputMode(1)

        val appVersion       = context.getString(R.string.app_version) + " " + Constants.APP_VERSION
        val author           = context.getString(R.string.author)
        val url              = Constants.AUTHOR
        textAutor.text       = author
        textAppVersion.text  = appVersion
        textTranslation.text = context.getString(R.string.dutch_translation)
        textDiane.text       = Constants.DIANE
        textMendanha.text    = Constants.MENDANHA

        textUrl.text = url
        if (!isAndroidTV(context)) {
            textUrl.setOnClickListener {
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(Constants.SERVER_URL + "/links")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    context.startActivity(i, ActivityOptions.makeSceneTransitionAnimation(context as Activity?).toBundle())
                } else {
                    context.startActivity(i)
                }
            }
        }

        btnAppGuide.setOnClickListener {
            showTipsDialog(context)
        }

        appInfo.setOnClickListener {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
            intent.data = uri
            context.startActivity(intent)
        }

        btnWebsite.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(Constants.APP_DOWNLOAD_URL))

            if (intent.resolveActivity(context.packageManager) != null) {
                context.startActivity(intent)
            } else {
                toast(context, context.getString(R.string.no_browser_available))
            }
        }

        btnReportError.setOnClickListener {
            dialogAbout.dismiss()
            reportError(context)
        }

        btnShare.setOnClickListener {
            shareAPP(context)
        }

        btnExit.setOnClickListener {
            dialogAbout.dismiss()
        }

        // Let's start with animation work. We just need to create a style and use it here as follows.
        if (dialogAbout.window != null)
            dialogAbout.window!!.attributes.windowAnimations = R.style.SlidingDialogAnimation
        dialogAbout.show()
    }

    private fun showTipsDialog(context: Context) {
        val inflater    = LayoutInflater.from(context)
        val dialogView  = inflater.inflate(R.layout.layout_tips, null)
        val alertDialog = android.app.AlertDialog.Builder(context)
            .setView(dialogView)
            .setCancelable(true)
            .setTitle(context.getString(R.string.fast_tips))
            .setIcon(R.drawable.ic_tips_and_updates_orange_24)
            .setNegativeButton(context.getString(R.string.close)) { dialog, _ ->
                dialog.dismiss()
            }
            .create()

        val webView = dialogView.findViewById<WebView>(R.id.webview)
        val htmlContent = context.getString(R.string.app_tips)
        webView.loadDataWithBaseURL(null, htmlContent, "text/html", "utf-8", null)

        if (alertDialog.window != null) {
            alertDialog.window!!.attributes.windowAnimations = R.style.SlidingDialogAnimation
        }

        alertDialog.show()
    }

    @JvmStatic
    fun openMessagesLayout(context: Context) {
        val messagesDialog = AlertDialog.Builder(context)
        messagesDialog.setTitle(context.getString(R.string.messages))
        messagesDialog.setCancelable(true)

        val inflater = LayoutInflater.from(context)
        val messagesView: View = inflater.inflate(R.layout.layout_messages, null)
        messagesDialog.setView(messagesView)

        val messageBatteryOn  = messagesView.findViewById<EditText>(R.id.messageEnergyOn)
        val messageBatteryOff = messagesView.findViewById<EditText>(R.id.messageEnergyOff)
        val btnAdd  = messagesView.findViewById<Button>(R.id.btn_add)
        val btnExit = messagesView.findViewById<Button>(R.id.btn_cancel)

        val alertDialog = messagesDialog.create()
        alertDialog.window!!.setSoftInputMode(1)

        val databaseHandler = DatabaseHandler(context)
        val getMessageOn = databaseHandler.viewMessage(1)
        val getMessageOff = databaseHandler.viewMessage(0)
        messageBatteryOn.setText(getMessageOn)
        messageBatteryOff.setText(getMessageOff)
        messageBatteryOn.requestFocus()

        if (getMessageOn.isNotEmpty() || getMessageOff.isNotEmpty()) {
            btnAdd.setText(R.string.update)
        } else {
            btnAdd.setText(R.string.add)
        }

        btnAdd.setOnClickListener {
            val getMessageBatteryOn  = messageBatteryOn.text.toString()
            val getMessageBatteryOff = messageBatteryOff.text.toString()

            if (getMessageBatteryOn.isNotEmpty() || getMessageBatteryOff.isNotEmpty()) {
                if (getMessageOn.isNotEmpty()) {
                    val status = databaseHandler.updateMessage(DrawMessageClass(1, getMessageBatteryOn))
                    if (status > -1) {
                        toast(context, context.getString(R.string.message_updated))
                    }
                } else {
                    val status = databaseHandler.addMessage(DrawMessageClass(1, getMessageBatteryOn), DrawMessageClass(1, getMessageBatteryOn))
                    if (status > -1) {
                        toast(context, context.getString(R.string.message_updated))
                    }
                }

                if (getMessageOff.isNotEmpty()) {
                    val status = databaseHandler.updateMessage(DrawMessageClass(0, getMessageBatteryOff))
                    if (status > -1) {
                        toast(context, context.getString(R.string.message_updated))
                    }
                } else {
                    val status = databaseHandler.addMessage(DrawMessageClass(0, getMessageBatteryOff), DrawMessageClass(0, getMessageBatteryOff))
                    if (status > -1) {
                        toast(context, context.getString(R.string.message_updated))
                    }
                }

                alertDialog.dismiss()
            } else {
                toast(context, context.getString(R.string.message_cannot_be_empty))
            }
        }

        btnExit.setOnClickListener {
            alertDialog.dismiss()
        }

        alertDialog.show()
    }

    @JvmStatic
    @SuppressLint("UseCompatLoadingForDrawables")
    private fun reportError(context: Context?) {
        val errorReportBox = android.app.AlertDialog.Builder(context)
        errorReportBox.setTitle(context!!.getString(R.string.report_error_or_suggestion))
        errorReportBox.setCancelable(false)
        errorReportBox.setIcon(R.drawable.ic_report_problem_24)

        val inflater = LayoutInflater.from(context)
        val errorReportDialog: View = inflater.inflate(R.layout.dialog_error_report, null)
        errorReportBox.setView(errorReportDialog)

        val edtEmail       = errorReportDialog.findViewById<TextInputLayout>(R.id.etEmail).editText
        val edtTitle       = errorReportDialog.findViewById<TextInputLayout>(R.id.etErrorTitle).editText
        val edtDescription = errorReportDialog.findViewById<TextInputLayout>(R.id.etErrorDescription).editText
        val btnSend        = errorReportDialog.findViewById<Button>(R.id.btnSendErrorReport)
        val btnCancel      = errorReportDialog.findViewById<Button>(R.id.btnCloseErrorReport)

        val dialogErrorReport = errorReportBox.create()
        dialogErrorReport.window!!.setSoftInputMode(1)

        btnCancel.setOnClickListener {
            dialogErrorReport.dismiss()
        }

        btnSend.setOnClickListener {
            val email   = edtEmail?.text.toString()
            val title   = edtTitle?.text.toString()
            val message = edtDescription?.text.toString()

            if (email.isNotEmpty() && title.isNotEmpty() && message.isNotEmpty()) {
                // Calls the server communication function to log in
                ErrorReport(context).reportError(email, title, message, { response ->
                    toast(context, response)
                    dialogErrorReport.dismiss()
                }, { error ->
                    // Show error message (communication failure, etc.)
                    if (BuildConfig.DEBUG) {
                        Log.e(TAG, "error: $error")
                    }
                    showSnackbarError(errorReportDialog, error)
                })
            } else {
                if (email.isEmpty()) {
                    edtEmail?.error = context.getString(R.string.empty)
                } else if (!validateEmail(email)) {
                    edtEmail?.error = context.getString(R.string.invalid_email)
                }

                if (title.isEmpty()) {
                    edtTitle?.error = context.getString(R.string.empty)
                }

                if (message.isEmpty()) {
                    edtDescription?.error = context.getString(R.string.empty)
                }
            }
        }

        edtEmail?.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // No need to implement
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Remove error message when user starts typing
                edtEmail.error = null
            }

            override fun afterTextChanged(s: Editable?) {
                // No need to implement
            }
        })

        edtTitle?.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // No need to implement
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Remove error message when user starts typing
                edtTitle.error = null
            }

            override fun afterTextChanged(s: Editable?) {
                // No need to implement
            }
        })

        edtDescription?.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // No need to implement
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Remove error message when user starts typing
                edtDescription.error = null
            }

            override fun afterTextChanged(s: Editable?) {
                // No need to implement
            }
        })

        if (dialogErrorReport.window != null) {
            dialogErrorReport.window!!.attributes.windowAnimations = R.style.SlidingDialogAnimation
        }

        dialogErrorReport.show()
    }

    @JvmStatic
    fun validateEmail(checkEmail: CharSequence?): Boolean {
        return checkEmail == null || !Patterns.EMAIL_ADDRESS.matcher(checkEmail).matches()
    }

    fun shareAPP(context: Context) {
        try {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.energy_check) + " " + context.getString(
                R.string.app_name
            ) +
                    "\n"+context.getString(R.string.get_your_app) + Constants.APP_DOWNLOAD_URL
            )
            sendIntent.type = "text/plain"
            context.startActivity(sendIntent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    fun isNightMode(context: Context): Boolean {
        when (context.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_YES -> {
                return true
            } Configuration.UI_MODE_NIGHT_NO -> {
                return false
            }
        }
        return false
    }

    fun showSnackbarNoInternet(view: View, msg: String) {
        val snackbar2 = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
        snackbar2.setAction(R.string.fix, OpenInternetSettings())
        snackbar2.show()
    }

    private fun showSnackbarError(view: View, msg: String) {
        val snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
        snackbar.setTextColor(ContextCompat.getColor(view.context, R.color.red))
        snackbar.setAction(R.string.close) {}
        snackbar.show()
    }

    fun showSnackbar(view: View, msg: String) {
        val snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(R.string.close) {}
        snackbar.show()
    }

    /**
     * Check for Virtual Machine
     */
    private fun String?.isGenymotionEmulator(): Boolean {
        return this != null &&
            (contains("Genymotion") || this == "unknown")
    }

    private fun String.buildModelContainsEmulatorHints(): Boolean {
        return (startsWith("sdk")
            || "google_sdk" == this || contains("Emulator")
            || contains("Android SDK"))
    }

    fun checkIfVirtualMachine(context: Context): Boolean {
        if (checkPreferencesExists(context, "checkIfVirtualMachine")) {
            return getSharedPreferencesBoolean(context, "checkIfVirtualMachine")
        }

        val inEmulator = "generic" == Build.BRAND.lowercase(Locale.getDefault())

        if (Build.MANUFACTURER.isGenymotionEmulator()) {
            setSharedPreferencesBoolean(context, "checkIfVirtualMachine", true)
            return true
        }

        if (Build.MODEL.buildModelContainsEmulatorHints()) {
            setSharedPreferencesBoolean(context, "checkIfVirtualMachine", true)
            return true
        }

        if (inEmulator) {
            setSharedPreferencesBoolean(context, "checkIfVirtualMachine", true)
            return true
        }

        setSharedPreferencesBoolean(context, "checkIfVirtualMachine", false)
        return false
    }
}