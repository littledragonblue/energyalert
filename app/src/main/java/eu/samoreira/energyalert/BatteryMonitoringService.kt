package eu.samoreira.energyalert

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Log
import eu.samoreira.energyalert.database.DatabaseHandler
import eu.samoreira.energyalert.utils.AppUtils
import eu.samoreira.energyalert.utils.Constants
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import kotlin.system.exitProcess

class BatteryMonitoringService: Service() {

    private val TAG = "TAG Battery Service: "
    private var isBatteryStatusRegistered = false
    private val databaseHandler = DatabaseHandler(this)
    private val sms = SmsService()
    private val email = EmailService()
    private val sound = SoundService()
    private val notificationHelper by lazy { NotificationHelper(this) }
    private var batteryStatus: Boolean = false
    private var didEnergyOffAlarmTrigger = false
    var energyStatus: Boolean = false

    private val delayHandler = Handler(Looper.getMainLooper())

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private val batteryStateReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Received intent with action: ${it.action}")
                }
                energyStatus = powerOnBatteryState(it)

                if (batteryStatus != energyStatus) {
                    val isWithinUserDefinedTimeRange = isWithinUserDefinedTimeRange(context!!)
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "isWithinUserDefinedTimeRange: $isWithinUserDefinedTimeRange")
                    }

                    if(!energyStatus) {
                        val defaultAlarmeDelayTime = (AppUtils.getSharedPreferencesInt(context, "alarmDelayTime", 0).toLong() * 60 * 1000)
                        saveLog(context,getString(R.string.energyOff) + " @ ${getCurrentDateTime()}")

                        updateStatusAndSendBroadcast(false, getString(R.string.energyOff), isWithinUserDefinedTimeRange)
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "start alarmRunnable: energyOff")
                        }

                        if (isWithinUserDefinedTimeRange) {
                            didEnergyOffAlarmTrigger = false
                            delayHandler.postDelayed(alarmRunnable, defaultAlarmeDelayTime)
                        }
                    } else {
                        delayHandler.removeCallbacks(alarmRunnable)

                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "delayHandler.removeCallbacks(alarmRunnable)")
                        }

                        saveLog(context,getString(R.string.energyOn) + " @ ${getCurrentDateTime()}")
                        updateStatusAndSendBroadcast(true, getString(R.string.energyOn), isWithinUserDefinedTimeRange)

                        if(didEnergyOffAlarmTrigger) {
                            if (isWithinUserDefinedTimeRange) {
                                val message: String =
                                    if (databaseHandler.viewMessage(1).isEmpty()) {
                                        getString(R.string.energyOn)
                                    } else {
                                        databaseHandler.viewMessage(1)
                                    }

                                sendAlertsBasedOnEnergyStatus(context, message)
                            }

                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "send Alarm: energyOn")
                            }
                            didEnergyOffAlarmTrigger = false
                        }
                    }
                }

                // Inform UI about the change using a broadcast
                val batteryStatusIntent = Intent("BATTERY_STATUS_CHANGED")
                batteryStatusIntent.putExtra("status", energyStatus)
                context!!.sendBroadcast(batteryStatusIntent)

                batteryStatus = energyStatus
            }
        }
    }

    private val alarmRunnable = Runnable {
        val message = if (databaseHandler.viewMessage(0).isEmpty()) {
            getString(R.string.energyOff)
        } else {
            databaseHandler.viewMessage(0)
        }

        sendAlertsBasedOnEnergyStatus(this, message)
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "send Alarm: energyOff")
        }
        didEnergyOffAlarmTrigger = true
    }

    private fun sendAlertsBasedOnEnergyStatus(context: Context?, message: String) {
        val getPhones = databaseHandler.viewPhones()
        val getEmails = databaseHandler.viewEmails()

        if (energyStatus) {
            sms.sendAlertSms(context!!, message, getPhones)
            email.sendEmail(context, message, getEmails)
            sound.sendSoundAlertEnergyON(context)
        } else {
            sms.sendAlertSms(context!!, message, getPhones)
            email.sendEmail(context, message, getEmails)
            sound.sendSoundAlertEnergyOFF(context, message)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            ACTION_START -> {
                start()
            }
            ACTION_STOP -> stopService()
            ACTION_FINISH_APP -> {
                stopService()
                exitProcess(0)
            }
        }

        return super.onStartCommand(intent, flags, startId)
    }

    private fun start() {
        val filter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)

        notificationHelper.createNotificationChannelImportanceHeight()
        val notification = notificationHelper.buildForegroundMainNotification()
        startForeground(Constants.NOTIFICATION_ID, notification)

        if (!isBatteryStatusRegistered) {
            isBatteryStatusRegistered = true
            ReceiverManager.register(this, batteryStateReceiver, filter)
        }
    }

    fun powerOnBatteryState(intent: Intent): Boolean {
        when (intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1)) {
            BatteryManager.BATTERY_PLUGGED_AC -> {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "on AC power")
                }
                return true
            } BatteryManager.BATTERY_PLUGGED_WIRELESS -> {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "on Wireless power")
                }
                return true
            } BatteryManager.BATTERY_PLUGGED_USB -> {
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "on USB power")
                }
                return true
            }
        }

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "on battery power")
        }
        return false
    }

    private fun updateStatusAndSendBroadcast(isCharging: Boolean, status: String, playSound: Boolean) {
        notificationHelper.updateNotification(getString(R.string.app_name), status, playSound)
        val updateIntent = Intent(ACTION_UPDATE_STATUS)
        updateIntent.putExtra("batteryStatus", isCharging)
        updateIntent.putExtra("subtitle", status)
        sendBroadcast(updateIntent)
    }

    override fun onDestroy() {
        super.onDestroy()

        delayHandler.removeCallbacksAndMessages(null)

        try {
            unregisterReceiver(batteryStateReceiver)
            isBatteryStatusRegistered = false
        } catch (e: IllegalArgumentException) {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "batteryStatusReceiver not registered", e)
            }
        }
    }

    companion object {
        const val ACTION_START = "ACTION_START"
        const val ACTION_STOP = "ACTION_STOP"
        const val ACTION_FINISH_APP = "ACTION_FINISH_APP"
        const val ACTION_UPDATE_STATUS = "ACTION_UPDATE_STATUS"
    }

    private fun stopService() {
        stopForeground(true)
        stopSelf()

        try {
            unregisterReceiver(batteryStateReceiver)
            isBatteryStatusRegistered = false
        } catch (e: IllegalArgumentException) {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "batteryStatusReceiver not registered", e)
            }
        }

        val stopActionStartedIntent = Intent(ACTION_STOP)
        sendBroadcast(stopActionStartedIntent)
    }

    private fun saveLog(context: Context, log: String) {
        val existingLogs = AppUtils.getSharedPreferencesString(context, "batteryLogs")
        val newLogs =  log + "\n" + existingLogs
        AppUtils.setSharedPreferencesString(context, "batteryLogs", newLogs)
    }

    private fun getCurrentDateTime(): String {
        val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
        return sdf.format(Date())
    }

    fun isWithinUserDefinedTimeRange(context: Context): Boolean {
        val currentTime = Calendar.getInstance()

        val startTimeString = AppUtils.getSharedPreferencesString(context, "startTime", "00:00")
        val endTimeString = AppUtils.getSharedPreferencesString(context, "endTime", "23:59")

        val startTime = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, startTimeString!!.split(":")[0].toInt())
            set(Calendar.MINUTE, startTimeString.split(":")[1].toInt())
        }

        val endTime = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, endTimeString!!.split(":")[0].toInt())
            set(Calendar.MINUTE, endTimeString.split(":")[1].toInt())
        }

        return currentTime.after(startTime) && currentTime.before(endTime)
    }
}