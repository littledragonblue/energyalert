package eu.samoreira.energyalert

import android.content.Intent
import android.provider.Settings
import android.view.View
import eu.samoreira.energyalert.utils.AppUtils

class OpenInternetSettings: View.OnClickListener {
    override fun onClick(v: View?) {
        if (AppUtils.isAndroidTV(v!!.context)) {
            val panelIntent = Intent(Settings.ACTION_WIFI_SETTINGS)
            v.context.startActivity(Intent(panelIntent))
        } else {
            v.context.startActivity(Intent(Settings.ACTION_WIRELESS_SETTINGS))
        }
    }
}