package eu.samoreira.energyalert

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import eu.samoreira.energyalert.database.DrawModelClass
import eu.samoreira.energyalert.ui.phone.PhoneFragment
import eu.samoreira.energyalert.utils.AppUtils

class PhoneAdapter(
	private val context: Context,
	private val phones: ArrayList<DrawModelClass>
) : RecyclerView.Adapter<PhoneAdapter.ViewHolder>() {

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		return ViewHolder(
			LayoutInflater.from(context).inflate(R.layout.items_row, parent, false)
		)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val item = phones[position]
		holder.txtDate.text = item.name
		holder.txtContact.text = item.contact

		// Detect Night Mode
		if (AppUtils.isNightMode(context)) {
			if (position % 2 == 0) {
				holder.llMain.setBackgroundColor(ContextCompat.getColor(context, R.color.purple_500))
			} else {
				holder.llMain.setBackgroundColor(ContextCompat.getColor(context, R.color.purple_700))
			}
		} else {
			if (position % 2 == 0) {
				holder.llMain.setBackgroundColor(ContextCompat.getColor(context, R.color.teal_200))
			} else {
				holder.llMain.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
			}
		}

		val savedPhonesFragment = PhoneFragment()

		holder.btnShare.setOnClickListener {
			savedPhonesFragment.shareContact(item, context as Activity)
		}

		holder.btnUpdate.setOnClickListener {
			savedPhonesFragment.updateContactDialog(item, context as Activity)
		}

		holder.btnDelete.setOnClickListener {
			savedPhonesFragment.deleteContactAlertDialog(item, context as Activity)
		}
	}
	class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
		val llMain: LinearLayout = view.findViewById(R.id.llMain)
		val txtDate: TextView = view.findViewById(R.id.txtDate)
		val txtContact: TextView = view.findViewById(R.id.txtContact)
		val btnShare: Button = view.findViewById(R.id.btn_share_draw)
		val btnUpdate: Button = view.findViewById(R.id.btn_update)
		val btnDelete: Button = view.findViewById(R.id.btn_delete)
	}

	override fun getItemCount() = phones.size
}