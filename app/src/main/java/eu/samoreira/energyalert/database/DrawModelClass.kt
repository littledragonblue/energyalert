package eu.samoreira.energyalert.database

// creating a Data Model Class
class DrawModelClass(val id: Int, val contact: String, val name: String)

class DrawMessageClass(val id: Int, val message: String)

class DrawEmailSettingsClass(val email: String, val host: String, val password: String, val smtp: String, val port: Int)