package eu.samoreira.energyalert.database

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteException
import android.util.Log
import eu.samoreira.energyalert.BuildConfig
import eu.samoreira.energyalert.EmailErrorReportService
import eu.samoreira.energyalert.utils.Constants

class DatabaseHandler(private val context: Context):
SQLiteOpenHelper(context, Constants.SQLITE_KEY, null, DATABASE_VERSION) {

	private val TAG = "TAG DatabaseHandler: "

	companion object {
		private const val DATABASE_VERSION = 1
		private const val TABLE_PHONES = "Phones"
		private const val TABLE_EMAILS = "Emails"
		private const val TABLE_MESSAGE = "Messages"
		private const val TABLE_EMAIL_SETTINGS = "EmailSettings"

		private const val KEY_ID = "_id"
		private const val KEY_PHONE = "phone"
		private const val KEY_EMAIL = "email"
		private const val KEY_DATE = "date"
		private const val KEY_MESSAGE = "message"
		private const val KEY_HOST = "host"
		private const val KEY_PASSWORD = "password"
		private const val KEY_SMTP = "smtp"
		private const val KEY_PORT = "port"
	}

	override fun onCreate(db: SQLiteDatabase?) {
		val createPhoneTable = ("CREATE TABLE " + TABLE_PHONES + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_PHONE + " TEXT,"
				+ KEY_DATE + " TEXT" + ")")
		db?.execSQL(createPhoneTable)

		val createEmailTable = ("CREATE TABLE " + TABLE_EMAILS + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_EMAIL + " TEXT,"
				+ KEY_DATE + " TEXT" + ")")
		db?.execSQL(createEmailTable)

		val createMessageTable = ("CREATE TABLE " + TABLE_MESSAGE + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_MESSAGE + " TEXT" + ")")
		db?.execSQL(createMessageTable)

		val createEmailSettingsTable = ("CREATE TABLE " + TABLE_EMAIL_SETTINGS + "("
				+ KEY_EMAIL + " TEXT PRIMARY KEY," + KEY_HOST + " TEXT,"
				+ KEY_PASSWORD + " TEXT," + KEY_SMTP + " TEXT," + KEY_PORT + " TEXT" + ")")
		db?.execSQL(createEmailSettingsTable)
	}

	fun dropDatabase() {
		val db = this.writableDatabase

		db!!.execSQL("DROP TABLE IF EXISTS $TABLE_PHONES")
		onCreate(db)

		db.execSQL("DROP TABLE IF EXISTS $TABLE_EMAILS")
		onCreate(db)

		db.execSQL("DROP TABLE IF EXISTS $TABLE_MESSAGE")
		onCreate(db)

		db.execSQL("DROP TABLE IF EXISTS $TABLE_EMAIL_SETTINGS")
		onCreate(db)
	}

	override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
		db!!.execSQL("DROP TABLE IF EXISTS $TABLE_PHONES")
		onCreate(db)

		db.execSQL("DROP TABLE IF EXISTS $TABLE_EMAILS")
		onCreate(db)

		db.execSQL("DROP TABLE IF EXISTS $TABLE_MESSAGE")
		onCreate(db)

		db.execSQL("DROP TABLE IF EXISTS $TABLE_EMAIL_SETTINGS")
		onCreate(db)
	}

	fun addPhone(phone: DrawModelClass): Long {
		val db = this.writableDatabase

		val contentValues = ContentValues()
		contentValues.put(KEY_PHONE, phone.contact)
		contentValues.put(KEY_DATE, phone.name)

		// Insert row
		val success = db.insert(TABLE_PHONES, null, contentValues)

		db.close()
		return success
	}

	fun addEmail(email: DrawModelClass): Long {
		val db = this.writableDatabase

		val contentValues = ContentValues()
		contentValues.put(KEY_EMAIL, email.contact)
		contentValues.put(KEY_DATE, email.name)

		// Insert row
		val success = db.insert(TABLE_EMAILS, null, contentValues)

		db.close()
		return success
	}

	fun addMessage(id: DrawMessageClass, message: DrawMessageClass): Long {
		val db = this.writableDatabase

		val contentValues = ContentValues()
		contentValues.put(KEY_ID, id.id)
		contentValues.put(KEY_MESSAGE, message.message)

		// Insert row
		val success = db.insert(TABLE_MESSAGE, null, contentValues)

		db.close()
		return success
	}

	fun addEmailSettings(email: DrawEmailSettingsClass, host: DrawEmailSettingsClass, password: DrawEmailSettingsClass,
						 smtp: DrawEmailSettingsClass, port: DrawEmailSettingsClass
	): Long {
		val db = this.writableDatabase

		val contentValues = ContentValues()
		contentValues.put(KEY_EMAIL, email.email)
		contentValues.put(KEY_HOST, host.host)
		contentValues.put(KEY_PASSWORD, password.password)
		contentValues.put(KEY_SMTP, smtp.smtp)
		contentValues.put(KEY_PORT, port.port)

		// Insert row
		val success = db.insert(TABLE_EMAIL_SETTINGS, null, contentValues)

		db.close()
		return success
	}

	@SuppressLint("Range", "Recycle")
	fun viewPhones(): ArrayList<DrawModelClass> {
		val phoneList: ArrayList<DrawModelClass> = ArrayList()
		val selectQuery = "SELECT * FROM $TABLE_PHONES"
		val db = this.readableDatabase
		val cursor: Cursor?

		try {
			cursor = db.rawQuery(selectQuery, null)
		} catch (e: SQLiteException) {
			db.execSQL(selectQuery)
			EmailErrorReportService().sendEmail(context, e.message.toString(), "DatabaseHandler.viewPhones()")
			db.close()
			return ArrayList()
		}

		var id: Int
		var phone: String
		var date: String

		if (cursor.moveToFirst()) {
			do {
				id = cursor.getInt(cursor.getColumnIndex(KEY_ID))
				phone = cursor.getString(cursor.getColumnIndex(KEY_PHONE))
				date = cursor.getString(cursor.getColumnIndex(KEY_DATE))

				val phones = DrawModelClass(id = id, contact = phone, name = date)
				phoneList.add(phones)
			} while (cursor.moveToNext())
		}

		db.close()
		return phoneList
	}

	@SuppressLint("Range", "Recycle")
	fun viewEmails(): ArrayList<DrawModelClass> {
		val emailList: ArrayList<DrawModelClass> = ArrayList()
		val selectQuery = "SELECT * FROM $TABLE_EMAILS"
		val db = this.readableDatabase
		val cursor: Cursor?

		try {
			cursor = db.rawQuery(selectQuery, null)
		} catch (e: SQLiteException) {
			db.execSQL(selectQuery)
			EmailErrorReportService().sendEmail(context, e.message.toString(), "DatabaseHandler.viewEmails()")
			db.close()
			return ArrayList()
		}

		var id: Int
		var email: String
		var date: String

		if (cursor.moveToFirst()) {
			do {
				id = cursor.getInt(cursor.getColumnIndex(KEY_ID))
				email = cursor.getString(cursor.getColumnIndex(KEY_EMAIL))
				date = cursor.getString(cursor.getColumnIndex(KEY_DATE))

				if (BuildConfig.DEBUG) {
					Log.d(TAG, email)
				}

				val emails = DrawModelClass(id = id, contact = email, name = date)
				emailList.add(emails)
			} while (cursor.moveToNext())
		}

		db.close()
		return emailList
	}

	@SuppressLint("Range", "Recycle")
	fun viewMessage(id: Int): String {
		val selectQuery = "SELECT message FROM $TABLE_MESSAGE WHERE $KEY_ID = $id"
		val db = this.readableDatabase
		val cursor: Cursor?

		try {
			cursor = db.rawQuery(selectQuery, null)
		} catch (e: SQLiteException) {
			db.execSQL(selectQuery)
			EmailErrorReportService().sendEmail(context, e.message.toString(), "DatabaseHandler.viewMessage()")
			db.close()
			return ""
		}

		if (cursor.moveToFirst()) {
			db.close()
			return cursor.getString(cursor.getColumnIndex(KEY_MESSAGE))
		}

		db.close()
		return ""
	}

	@SuppressLint("Range", "Recycle")
	fun viewEmailSettings(): ArrayList<DrawEmailSettingsClass> {
		val settingsList: ArrayList<DrawEmailSettingsClass> = ArrayList()
		val selectQuery = "SELECT * FROM $TABLE_EMAIL_SETTINGS"
		val db = this.readableDatabase
		val cursor: Cursor?

		try {
			cursor = db.rawQuery(selectQuery, null)
		} catch (e: SQLiteException) {
			db.execSQL(selectQuery)
			EmailErrorReportService().sendEmail(context, e.message.toString(), "DatabaseHandler.viewEmailSettings()")
			db.close()
			return ArrayList()
		}

		if (cursor.moveToFirst()) {
			val email: String = cursor.getString(cursor.getColumnIndex(KEY_EMAIL))
			val host: String = cursor.getString(cursor.getColumnIndex(KEY_HOST))
			val password: String = cursor.getString(cursor.getColumnIndex(KEY_PASSWORD))
			val smtp: String = cursor.getString(cursor.getColumnIndex(KEY_SMTP))
			val port: Int = cursor.getInt(cursor.getColumnIndex(KEY_PORT))

			val settings = DrawEmailSettingsClass(email = email, host = host, password = password, smtp = smtp, port = port)
			settingsList.add(settings)
		}

		db.close()
		return settingsList
	}

	fun updatePhone(phone: DrawModelClass): Int {
		val db = this.writableDatabase
		val contentValues = ContentValues()
		contentValues.put(KEY_PHONE, phone.contact)
		contentValues.put(KEY_DATE, phone.name)

		val success = db.update(TABLE_PHONES, contentValues, KEY_ID + "=" + phone.id, null)

		db.close()
		return success
	}

	fun updateEmail(email: DrawModelClass): Int {
		val db = this.writableDatabase
		val contentValues = ContentValues()
		contentValues.put(KEY_EMAIL, email.contact)
		contentValues.put(KEY_DATE, email.name)

		val success = db.update(TABLE_PHONES, contentValues, KEY_ID + "=" + email.id, null)

		db.close()
		return success
	}

	fun updateMessage(message: DrawMessageClass): Int {
		val db = this.writableDatabase
		val contentValues = ContentValues()
		contentValues.put(KEY_MESSAGE, message.message)

		val success = db.update(TABLE_MESSAGE, contentValues, KEY_ID + "=" + message.id, null)

		db.close()
		return success
	}

	fun updateEmailSettings(email: DrawEmailSettingsClass, host: DrawEmailSettingsClass, password: DrawEmailSettingsClass,
							smtp: DrawEmailSettingsClass, port: DrawEmailSettingsClass
	): Int {
		val db = this.writableDatabase
		val contentValues = ContentValues()
		contentValues.put(KEY_EMAIL, email.email)
		contentValues.put(KEY_HOST, host.host)
		contentValues.put(KEY_PASSWORD, password.password)
		contentValues.put(KEY_SMTP, smtp.smtp)
		contentValues.put(KEY_PORT, port.port)

		val success = db.update(TABLE_EMAIL_SETTINGS, contentValues, KEY_EMAIL + "=" + email.email, null)

		db.close()
		return success
	}

	fun deletePhone(phone: DrawModelClass): Int {
		val db = this.writableDatabase
		val contentValues = ContentValues()
		contentValues.put(KEY_ID, phone.id)

		val success = db.delete(TABLE_PHONES, KEY_ID + "=" + phone.id, null)

		db.close()
		return success
	}

	fun deleteEmail(email: DrawModelClass): Int {
		val db = this.writableDatabase
		val contentValues = ContentValues()
		contentValues.put(KEY_ID, email.id)

		val success = db.delete(TABLE_EMAILS, KEY_ID + "=" + email.id, null)

		db.close()
		return success
	}

	fun deleteMessage(message: DrawMessageClass): Int {
		val db = this.writableDatabase
		val contentValues = ContentValues()
		contentValues.put(KEY_ID, message.id)

		val success = db.delete(TABLE_MESSAGE, KEY_ID + "=" + message.id, null)

		db.close()
		return success
	}

	fun deleteEmailSettings(email: DrawEmailSettingsClass): Int {
		val db = this.writableDatabase
		val contentValues = ContentValues()
		contentValues.put(KEY_EMAIL, email.email)

		val success = db.delete(TABLE_EMAIL_SETTINGS, KEY_ID + "=" + email.email, null)

		db.close()
		return success
	}
}