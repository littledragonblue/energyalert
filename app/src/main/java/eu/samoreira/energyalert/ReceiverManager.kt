package eu.samoreira.energyalert

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.os.Build
import android.content.Context.RECEIVER_EXPORTED
import android.util.Log

object ReceiverManager {
    private val receivers = mutableListOf<BroadcastReceiver>()
    private const val TAG = "TAG ReceiverManager: "

    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    fun register(context: Context, receiver: BroadcastReceiver, filter: IntentFilter) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            context.registerReceiver(receiver, filter, RECEIVER_EXPORTED)
        } else {
            context.registerReceiver(receiver, filter)
        }

        receivers.add(receiver)
    }

    fun unregisterAll(context: Context) {
        val iterator = receivers.iterator()

        while (iterator.hasNext()) {
            val receiver = iterator.next()

            try {
                context.unregisterReceiver(receiver)
                iterator.remove()
            } catch (e: IllegalArgumentException) {
                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "Receiver not registered", e)
                }
            }
        }
    }
}