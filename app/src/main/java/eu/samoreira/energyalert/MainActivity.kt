package eu.samoreira.energyalert

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.drawable.Drawable
import android.graphics.drawable.InsetDrawable
import android.os.BatteryManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.multidex.MultiDex
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import eu.samoreira.energyalert.databinding.ActivityMainBinding
import eu.samoreira.energyalert.utils.AppUtils

class MainActivity: AppCompatActivity() {

    private val TAG = "TAG MAINACTIVITY"
    private lateinit var binding: ActivityMainBinding
    private var isBatteryServiceRegistered: Boolean = false
    private var batteryIcon: MenuItem? = null

    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Set the Toolbar as ActionBar
        setSupportActionBar(binding.toolbar)
        binding.toolbar.logo = getLogoWithPadding(R.drawable.logo_24)
        binding.toolbar.subtitle = getString(R.string.app_name)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_phone_list, R.id.navigation_email_list
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        PermissionHelper.checkAndRequestPermissions(this as Activity)
        NotificationHelper(this).createNotificationChannelImportanceLow()

        startBatteryServices(this)
    }

    private fun getLogoWithPadding(@DrawableRes logoRes: Int): Drawable {
        val logo = ResourcesCompat.getDrawable(resources, logoRes, null)
        return InsetDrawable(logo, 0, 0, resources.getDimensionPixelSize(R.dimen.padding_medium), 0)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_BACK, KeyEvent.KEYCODE_ESCAPE, KeyEvent.KEYCODE_BUTTON_B, KeyEvent.KEYCODE_HOME -> {
                AppUtils.closeApp(this)
                return true
            }
        }

        return super.onKeyUp(keyCode, event)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        batteryIcon = menu.findItem(R.id.status_battery)

        // Update the battery icon based on the current battery status
        changeBatteryIconToolbar(getBatteryStatus(this))
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_about -> {
                AppUtils.openAboutLayout(this)
                return true
            } R.id.action_message -> {
                AppUtils.openMessagesLayout(this)
                return true
            } R.id.action_email -> {
                AddServer.abrirEmail(this)
                return true
            } R.id.action_server -> {
                AddServer.addServerDialog(this)
                return true
            } R.id.action_logs -> {
                showLogsDialog()
                return true
            } R.id.action_share -> {
                AppUtils.shareAPP(this)
                return true
            } R.id.action_exit -> {
                AppUtils.closeApp(this)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun startBatteryServices(context: Context?) {
        try {
            if (!isBatteryServiceRegistered) {
                Intent(context, BatteryMonitoringService::class.java).apply {
                    action = BatteryMonitoringService.ACTION_START
                    context?.startService(this)
                }

                isBatteryServiceRegistered = true

                if (BuildConfig.DEBUG) {
                    Log.e(TAG, "isBatteryServiceRegistered = true")
                }
            }
        } catch (e: IllegalArgumentException) {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "Receiver not registered", e)
            }

            EmailErrorReportService().sendEmail(this, e.message.toString(), "HomeFragment.startBatteryServices()")
        }
    }


    private val batteryStatusReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val status = intent?.getBooleanExtra("status", false) ?: false

            if (BuildConfig.DEBUG) {
                Log.e(TAG, "batteryStatusReceiver: $status")
            }

            changeBatteryIconToolbar(status)
        }
    }

    private fun changeBatteryIconToolbar(energyStatus: Boolean) {
        if (energyStatus) {
            batteryIcon?.setIcon(R.drawable.battery_green)
        } else {
            batteryIcon?.setIcon(R.drawable.battery_red)
        }
    }

    private fun getBatteryStatus(context: Context): Boolean {
        val batteryStatus: Intent? = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
            context.registerReceiver(null, ifilter)
        }

        val status: Int = batteryStatus?.getIntExtra(BatteryManager.EXTRA_STATUS, -1) ?: -1
        return status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL
    }

    private fun showLogsDialog() {
        val logs = AppUtils.getSharedPreferencesString(this, "batteryLogs")
        val alertDialog = AlertDialog.Builder(this)
            .setTitle(R.string.logs)
            .setMessage(logs)
            .setIcon(R.drawable.ic_logs_24)
            .setPositiveButton(R.string.close) { dialog, _ -> dialog.dismiss() }
            .setNegativeButton(R.string.clear_logs) { dialog, _ ->
                AppUtils.setSharedPreferencesString(this, "batteryLogs", "")
                dialog.dismiss()
                AppUtils.toast(this, getString(R.string.logs_cleared))
            }
            .create()

        alertDialog.show()
    }


    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter("BATTERY_STATUS_CHANGED")

        ReceiverManager.register(this, batteryStatusReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()

        try {
            unregisterReceiver(batteryStatusReceiver)
        } catch (e: IllegalArgumentException) {
            if (BuildConfig.DEBUG) {
                Log.e(TAG, "batteryStatusReceiver not registered", e)
            }
        }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onDestroy() {
        super.onDestroy()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask()
        } else {
            finish()
        }
    }
}