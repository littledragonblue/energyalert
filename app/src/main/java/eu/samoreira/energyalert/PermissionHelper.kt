package eu.samoreira.energyalert

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import eu.samoreira.energyalert.utils.Constants

object PermissionHelper {

    private val permissionsToRequest = mutableListOf<String>()

    fun checkAndRequestPermissions(activity: Activity) {
        permissionsToRequest.clear()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            permissionsToRequest.addAll(
                listOf(
                    Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
                    Manifest.permission.SYSTEM_ALERT_WINDOW,
                    Manifest.permission.DISABLE_KEYGUARD,
                    Manifest.permission.WAKE_LOCK,
                    Manifest.permission.VIBRATE,
                    Manifest.permission.BATTERY_STATS
                )
            )
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            permissionsToRequest.addAll(
                listOf(
                    Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
                    Manifest.permission.SYSTEM_ALERT_WINDOW,
                    Manifest.permission.VIBRATE,
                    Manifest.permission.BATTERY_STATS
                )
            )
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            permissionsToRequest.addAll(
                listOf(
                    Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
                    Manifest.permission.WAKE_LOCK,
                )
            )
        }

        val permissionsNotGranted = permissionsToRequest.filterNot {
            when (it) {
                Manifest.permission.SYSTEM_ALERT_WINDOW -> hasOverlayPermission(activity)
                else -> ContextCompat.checkSelfPermission(activity, it) == PackageManager.PERMISSION_GRANTED
            }
        }

        val normalPermissions = permissionsNotGranted.filter { it != Manifest.permission.SYSTEM_ALERT_WINDOW }.toTypedArray()

        if (normalPermissions.isNotEmpty()) {
            ActivityCompat.requestPermissions(activity, normalPermissions, Constants.PERMISSION_REQUEST_CODE)
        }

        if (!hasOverlayPermission(activity)) {
            showOverlayPermissionDialog(activity)
        }

        // Check and request SCHEDULE_EXACT_ALARM separately
        //checkAndRequestScheduleExactAlarmPermission(activity)
    }

    fun checkAndRequestNotificationsPermissions(activity: Activity): Boolean {
        permissionsToRequest.clear()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            permissionsToRequest.addAll(
                listOf(
                    Manifest.permission.POST_NOTIFICATIONS,
                    Manifest.permission.FOREGROUND_SERVICE,
                )
            )
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            permissionsToRequest.addAll(
                listOf(
                    Manifest.permission.FOREGROUND_SERVICE,
                )
            )
        } else {
            return true
        }

        val permissionsNotGranted = permissionsToRequest.filter {
            ContextCompat.checkSelfPermission(activity, it) != PackageManager.PERMISSION_GRANTED
        }

        if (permissionsNotGranted.isNotEmpty()) {
            // Ask permissions
            ActivityCompat.requestPermissions(activity, permissionsNotGranted.toTypedArray(), Constants.PERMISSION_REQUEST_CODE)
            return false
        }

        return true
    }

    private fun hasOverlayPermission(context: Context): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Settings.canDrawOverlays(context)
        } else {
            true
        }
    }

    private fun requestOverlayPermission(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val runnable = Runnable {
                val intent = Intent(
                    Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + context.packageName)
                )
                if (context is Activity) {
                    context.startActivityForResult(intent, Constants.PERMISSION_REQUEST_CODE)
                } else {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    context.startActivity(intent)
                }
            }

            Handler(Looper.getMainLooper()).postDelayed(runnable, 7000)
        }
    }

    private fun showOverlayPermissionDialog(context: Context) {
        AlertDialog.Builder(context)
            .setTitle(context.getString(R.string.permission_required))
            .setMessage(context.getString(R.string.ask_overlay_permission))
            .setPositiveButton(context.getString(R.string.grant)) { _, _ ->
                requestOverlayPermission(context)
            }
            .setNegativeButton(context.getString(R.string.exit), null)
            .show()
    }

    fun hasMultiplePermissions(context: Context, vararg permissions: String): Boolean {
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    fun requestMultiplePermissions(activity: Activity, requestCode: Int, vararg permissions: String) {
        ActivityCompat.requestPermissions(activity, permissions, requestCode)
    }

    @RequiresApi(Build.VERSION_CODES.S)
    fun showScheduleExactAlarmPermissionDialog(activity: Activity) {
        activity.runOnUiThread {
            AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.permission_required))
                .setMessage(activity.getString(R.string.permission_text_details_alarm))
                .setPositiveButton(activity.getString(R.string.settings)) { dialog, which ->
                    // Guide user to system settings to manually grant permission
                    val intent = Intent(Settings.ACTION_REQUEST_SCHEDULE_EXACT_ALARM)
                    activity.startActivity(intent)
                }
                .setNegativeButton(activity.getString(R.string.cancel), null)
                .show()
        }
    }

    private fun checkAndRequestScheduleExactAlarmPermission(activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.SCHEDULE_EXACT_ALARM) != PackageManager.PERMISSION_GRANTED) {
                // Show explanation dialog before directing to settings
                showScheduleExactAlarmPermissionDialog(activity)
            }
        }
    }
}