package eu.samoreira.energyalert

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import eu.samoreira.energyalert.database.DatabaseHandler
import eu.samoreira.energyalert.database.DrawEmailSettingsClass
import eu.samoreira.energyalert.utils.AppUtils
import eu.samoreira.energyalert.utils.Constants
import java.security.cert.CertificateException

object AddServer {
    private const val TAG = "TAG AddServer: "

    fun getServerUrl(context: Context?): String {
        val useHttps = AppUtils.getSharedPreferencesBoolean(context, "useHttps")

        return if (AppUtils.checkIfVirtualMachine(context!!)) {
            Constants.TEST_SERVER_URL
        } else {
            if (useHttps) {
                Constants.SERVER_URL
            } else {
                Constants.SERVER_HTTP_URL
            }
        }
    }

    private fun setServerUrl(context: Context?, string: String) {
        val sharedPreferences = context?.applicationContext?.getSharedPreferences(Constants.SHARED_PREFERENCES_KEY, 0)
        val editor = sharedPreferences?.edit()
        editor?.putString("serverUrl", string)
        editor?.apply()
    }


    /**
     * Dialogs
     */
    @JvmStatic
    @SuppressLint("UseCompatLoadingForDrawables")
    fun abrirEmail(context: Context) {
        val caixaDialogo = AlertDialog.Builder(context)
        caixaDialogo.setTitle(context.getString(R.string.email_settings))
        caixaDialogo.setCancelable(true)
        caixaDialogo.setIcon(R.drawable.ic_email)

        val inflater = LayoutInflater.from(context)
        val dialogoEmailSettings: View = inflater.inflate(R.layout.layout_email_config, null)
        caixaDialogo.setView(dialogoEmailSettings)

        val emailAddress = dialogoEmailSettings.findViewById<EditText>(R.id.email)
        val host         = dialogoEmailSettings.findViewById<EditText>(R.id.host)
        val password     = dialogoEmailSettings.findViewById<EditText>(R.id.password)
        val SMTPSecure   = dialogoEmailSettings.findViewById<EditText>(R.id.SMTPSecure)
        val port         = dialogoEmailSettings.findViewById<EditText>(R.id.port)
        val btnAdd       = dialogoEmailSettings.findViewById<Button>(R.id.btn_add)
        val btnExit      = dialogoEmailSettings.findViewById<Button>(R.id.btn_cancel)

        val dialogoSobre = caixaDialogo.create()
        dialogoSobre.window!!.setSoftInputMode(1)

        val databaseHandler  = DatabaseHandler(context)
        val getEmailSettings = databaseHandler.viewEmailSettings()

        if (getEmailSettings.size > 0) {
            emailAddress.setText(getEmailSettings[0].email)
            host.setText(getEmailSettings[0].host)
            password.setText(getEmailSettings[0].password)
            SMTPSecure.setText(getEmailSettings[0].smtp)
            port.setText(getEmailSettings[0].port.toString())
            btnAdd.setText(R.string.update)
        } else {
            btnAdd.setText(R.string.add)
        }

        btnAdd.setOnClickListener {
            val getEmailAddress: String = emailAddress.text.toString()
            val getHost: String         = host.text.toString()
            val getPassword: String     = password.text.toString()
            val getSMTPSecure: String   = SMTPSecure.text.toString()
            val getPort: String         = port.text.toString()

            if (getEmailAddress.isNotEmpty() && getHost.isNotEmpty() && getPassword.isNotEmpty() && getPassword.isNotEmpty() && getSMTPSecure.isNotEmpty() && getPort.isNotEmpty()) {
                if (AppUtils.validateEmail(getEmailAddress)) {
                    emailAddress.error = context.getString(R.string.invalid_email)
                } else {
                    if (getEmailSettings.size > 0) {
                        val status = databaseHandler.updateEmailSettings(
                            DrawEmailSettingsClass(getEmailAddress, getHost, getPassword, getSMTPSecure, getPort.toInt()),
                            DrawEmailSettingsClass(getEmailAddress, getHost, getPassword, getSMTPSecure, getPort.toInt()),
                            DrawEmailSettingsClass(getEmailAddress, getHost, getPassword, getSMTPSecure, getPort.toInt()),
                            DrawEmailSettingsClass(getEmailAddress, getHost, getPassword, getSMTPSecure, getPort.toInt()),
                            DrawEmailSettingsClass(getEmailAddress, getHost, getPassword, getSMTPSecure, getPort.toInt())
                        )
                        if (status > -1) {
                            AppUtils.toast(context, context.getString(R.string.message_updated))
                        }
                    } else {
                        val status = databaseHandler.addEmailSettings(
                            DrawEmailSettingsClass(getEmailAddress, getHost, getPassword, getSMTPSecure, getPort.toInt()),
                            DrawEmailSettingsClass(getEmailAddress, getHost, getPassword, getSMTPSecure, getPort.toInt()),
                            DrawEmailSettingsClass(getEmailAddress, getHost, getPassword, getSMTPSecure, getPort.toInt()),
                            DrawEmailSettingsClass(getEmailAddress, getHost, getPassword, getSMTPSecure, getPort.toInt()),
                            DrawEmailSettingsClass(getEmailAddress, getHost, getPassword, getSMTPSecure, getPort.toInt())
                        )
                        if (status > -1) {
                            AppUtils.toast(context, context.getString(R.string.message_updated))
                        }
                    }
                }

                dialogoSobre.dismiss()
            } else if (AppUtils.validateEmail(getEmailAddress) || getEmailAddress.isEmpty()) {
                AppUtils.toast(context, context.getString(R.string.invalid_email))
                emailAddress.requestFocus()
            } else if (getHost.isEmpty()) {
                AppUtils.toast(context, "Host: " + context.getString(R.string.cannot_be_empty))
                host.requestFocus()
            } else if (getPassword.isEmpty()) {
                AppUtils.toast(
                    context,context.getString(R.string.password) +
                    ": " + context.getString(R.string.cannot_be_empty)
                )
                password.requestFocus()
            } else if (getSMTPSecure.isEmpty()) {
                AppUtils.toast(
                    context,
                    "SMTPSecure: " + context.getString(R.string.cannot_be_empty)
                )
                SMTPSecure.requestFocus()
            } else if (getPort.isEmpty()) {
                AppUtils.toast(context, context.getString(R.string.port) + ": " + context.getString(R.string.cannot_be_empty))
                port.requestFocus()
            }
        }

        btnExit.setOnClickListener {
            dialogoSobre.dismiss()
        }

        dialogoSobre.show()
    }

    @JvmStatic
    @SuppressLint("UseCompatLoadingForDrawables")
    fun addServerDialog(context: Context) {
        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle(context.getString(R.string.server_url))
        alertDialog.setCancelable(true)
        alertDialog.setIcon(R.drawable.logo_24)

        val inflater = LayoutInflater.from(context)
        val layoutLogin: View = inflater.inflate(R.layout.layout_add_server, null)
        alertDialog.setView(layoutLogin)

        val serverUrl  = layoutLogin.findViewById<EditText>(R.id.server_url)
        val btnAdd     = layoutLogin.findViewById<Button>(R.id.btn_add)
        val btnCancel  = layoutLogin.findViewById<Button>(R.id.btn_cancel)

        val dialogServer = alertDialog.create()
        dialogServer.window!!.setSoftInputMode(1)

        serverUrl.setText(getServerUrl(context))
        btnAdd.setOnClickListener {
            val getServerUrl: String = serverUrl.text.toString()

            setServerUrl(context, getServerUrl)
            dialogServer.dismiss()
        }

        btnCancel.setOnClickListener {
            dialogServer.dismiss()
        }

        // Let's start with animation work. We just need to create a style and use it here as follows.
        if (dialogServer.window != null)
            dialogServer.window!!.attributes.windowAnimations = R.style.SlidingDialogAnimation
        dialogServer.show()
    }

    @Suppress("unused", "unused", "unused")
    fun checkHttps(context: Context, callback: (Boolean) -> Unit) {
        val queue = Volley.newRequestQueue(context)
        val url = Constants.SERVER_URL + "/meucalendario/?check_https"

        val stringRequest = StringRequest(
            Request.Method.GET, url, { response ->
                AppUtils.setSharedPreferencesBoolean(context, "useHttps", true)
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "Using HTTPS: -> $response")
                }
                callback(true)
            },
            { error ->
                if (isSSLError(error)) {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Problem with SSL Certificate")
                    }
                    AppUtils.setSharedPreferencesBoolean(context, "useHttps", false)
                    callback(false)
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "Unknown error while checking HTTPS support")
                    }
                    callback(false)
                }
            }
        )

        queue.add(stringRequest)
    }

    private fun isSSLError(error: VolleyError): Boolean {
        var throwable: Throwable? = error.cause
        while (throwable != null) {
            if (throwable is CertificateException) {
                return true
            }
            throwable = throwable.cause
        }
        return false
    }
}