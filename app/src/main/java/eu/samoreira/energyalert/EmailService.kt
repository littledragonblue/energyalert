package eu.samoreira.energyalert

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import eu.samoreira.energyalert.database.DatabaseHandler
import eu.samoreira.energyalert.database.DrawModelClass
import eu.samoreira.energyalert.utils.AppUtils
import eu.samoreira.energyalert.utils.Constants

class EmailService: Service() {
    private val TAG = "TAG EmailService"

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    fun sendEmail(context: Context, message: String, getEmails: ArrayList<DrawModelClass>) {
        if (!Constants.isEmailActive) return

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "sendEmail()")
        }

        val databaseHandler = DatabaseHandler(context)
        if (databaseHandler.viewEmailSettings().isEmpty()) return

        val emailMessage = context.getString(R.string.app_name) + ": " + message
        var stringEmail = ""

        for (i in 1..getEmails.size) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, getEmails[(i - 1)].contact)
            }

            stringEmail = if (i != getEmails.size) {
                stringEmail + getEmails[(i - 1)].contact + ","
            } else {
                stringEmail + getEmails[(i - 1)].contact
            }
        }

        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(context)
        val url = AddServer.getServerUrl(context) + Constants.EMAIL_SERVICE_URL + "&email=1&isandroid=yes&appversion=" + Constants.APP_VERSION

        if (BuildConfig.DEBUG) {
            Log.d(TAG, "url: $url")
            Log.d(TAG, "stringEmail: $stringEmail")
            Log.d(TAG, "emailMessage: $emailMessage")
        }

        // Request a string response from the provided URL.
        val stringRequest: StringRequest = object: StringRequest(Method.POST, url, Response.Listener { response ->
            AppUtils.toast(context, "Email Sent: $response")
        }, Response.ErrorListener {
            AppUtils.toast(context, "That didn't work! Email Settings Problem")
        }) {
            override fun getParams(): Map<String, String> {
                val postParam: MutableMap<String, String> = java.util.HashMap()
                postParam["host"] = databaseHandler.viewEmailSettings()[0].host
                postParam["username"] = databaseHandler.viewEmailSettings()[0].email
                postParam["password"] = databaseHandler.viewEmailSettings()[0].password
                postParam["SMTPSecure"] = databaseHandler.viewEmailSettings()[0].smtp
                postParam["port"] = databaseHandler.viewEmailSettings()[0].port.toString()
                postParam["arrayEmails"] = stringEmail
                postParam["message"] = emailMessage
                postParam["app_key"] = BuildConfig.APP_KEY
                return postParam
            }
        }

        databaseHandler.close()
        queue.add(stringRequest)
    }
}