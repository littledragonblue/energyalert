package eu.samoreira.energyalert

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import eu.samoreira.energyalert.database.DrawModelClass
import eu.samoreira.energyalert.ui.email.EmailFragment
import eu.samoreira.energyalert.utils.AppUtils

class EmailAdapter(
	private val context: Context,
	private val emails: ArrayList<DrawModelClass>
) : RecyclerView.Adapter<EmailAdapter.ViewHolder>() {

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		return ViewHolder(
			LayoutInflater.from(context).inflate(R.layout.items_row, parent, false)
		)
	}

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val item = emails[position]
		holder.txtDate.text = item.name
		holder.txtEmail.text = item.contact

		// Detect Night Mode
		if (AppUtils.isNightMode(context)) {
			if (position % 2 == 0) {
				holder.llMain.setBackgroundColor(ContextCompat.getColor(context, R.color.purple_500))
			} else {
				holder.llMain.setBackgroundColor(ContextCompat.getColor(context, R.color.purple_700))
			}
		} else {
			if (position % 2 == 0) {
				holder.llMain.setBackgroundColor(ContextCompat.getColor(context, R.color.teal_200))
			} else {
				holder.llMain.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
			}
		}

		val savedEmailsFragment = EmailFragment()

		holder.btnShare.setOnClickListener {
			savedEmailsFragment.shareContact(item, context as Activity)
		}

		holder.btnUpdate.setOnClickListener {
			savedEmailsFragment.updateContactDialog(item, context as Activity)
		}

		holder.btnDelete.setOnClickListener {
			savedEmailsFragment.deleteContactAlertDialog(item, context as Activity)
		}
	}
	class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
		val llMain: LinearLayout = view.findViewById(R.id.llMain)
		val txtDate: TextView = view.findViewById(R.id.txtDate)
		val txtEmail: TextView = view.findViewById(R.id.txtContact)
		val btnShare: Button = view.findViewById(R.id.btn_share_draw)
		val btnUpdate: Button = view.findViewById(R.id.btn_update)
		val btnDelete: Button = view.findViewById(R.id.btn_delete)
	}

	override fun getItemCount() = emails.size
}