package eu.samoreira.energyalert

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import eu.samoreira.energyalert.utils.Constants

class NotificationHelper(private val context: Context) {

    private val TAG = "TAG NotificationHelper"

    init {
        createNotificationChannelImportanceLow()
        createNotificationChannelImportanceHeight()
    }

    // Create the NotificationChannel, but only on API 26+ (Android Oreo) because
    // the NotificationChannel class is new and not in the support library.
    fun createNotificationChannelImportanceLow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(Constants.CHANNEL_SERVICE_ID, Constants.CHANNEL_SERVICE_NAME, NotificationManager.IMPORTANCE_LOW).apply {
                description = Constants.CHANNEL_DESCRIPTION
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "createNotificationChannelImportanceLow()")
            }
        }
    }

    fun createNotificationChannelImportanceHeight() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(Constants.CHANNEL_ALARM_ID, Constants.CHANNEL_ALARM_NAME, NotificationManager.IMPORTANCE_HIGH)
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)

            if (BuildConfig.DEBUG) {
                Log.d(TAG, "createNotificationChannelImportanceHeight()")
            }
        }
    }

    fun updateNotification(title: String = context.getString(R.string.app_name), message: String = context.getString(R.string.running_battery_status), playSound: Boolean = true) {
        val notification = buildForegroundMainNotification(title, message, playSound)
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(Constants.NOTIFICATION_ID, notification)
    }

    fun buildForegroundMainNotification(title: String = context.getString(R.string.app_name), message: String = context.getString(R.string.running_battery_status), playSound: Boolean = true): Notification {
        val openAppIntent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        }
        val openAppPendingIntent = PendingIntent.getActivity(context, Constants.PERMISSION_REQUEST_CODE, openAppIntent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)
        val notificationBuilder: NotificationCompat.Builder

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { // API 23
            val finishAppIntent = Intent(context, BatteryMonitoringService::class.java).apply {
                action = BatteryMonitoringService.ACTION_FINISH_APP
            }
            val finishPomodoroPendingIntent = PendingIntent.getService(context, Constants.PERMISSION_REQUEST_CODE, finishAppIntent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)

            notificationBuilder = NotificationCompat.Builder(context, Constants.CHANNEL_ALARM_ID)
                .setContentTitle(title)
                .setContentText(message)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setColorized(true)
                .setCategory(BatteryMonitoringService.ACTION_START)
                .setSmallIcon(R.drawable.logo_24)
                .setOngoing(true)
                .setContentIntent(openAppPendingIntent)
                .addAction(R.drawable.ic_exit, context.getString(R.string.exit), finishPomodoroPendingIntent)
        } else {
            notificationBuilder = NotificationCompat.Builder(context, Constants.CHANNEL_ALARM_ID)
                .setContentTitle(title)
                .setContentText(message)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSmallIcon(R.drawable.logo_24)
                .setOngoing(true)
                .setContentIntent(openAppPendingIntent)
        }

        if (!playSound) {
            notificationBuilder.setSound(null)
        }

        return notificationBuilder.build()
    }
}