package eu.samoreira.energyalert

import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.IBinder
import android.os.VibrationEffect
import android.os.Vibrator
import android.os.VibratorManager
import eu.samoreira.energyalert.utils.Constants

class SoundService: Service() {

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        return
    }

    fun sendSoundAlertEnergyOFF(context: Context, message: String) {
        if (!Constants.isSoundActive) return

        Thread {
            val intent = Intent(context, AlarmSoundActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("message", message)
            context.startActivity(intent)
            startVibration(context)
        }.start()
    }

    fun sendSoundAlertEnergyON(context: Context) {
        if (!Constants.isSoundActive) return

        startVibration(context)

        val notification: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val r = RingtoneManager.getRingtone(context, notification)
        return r.play()
    }

    private fun startVibration(context: Context) {
        val vib = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            val vibratorManager = context.getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager
            vibratorManager.defaultVibrator
        } else {
            @Suppress("DEPRECATION")
            context.getSystemService(VIBRATOR_SERVICE) as Vibrator
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vib.vibrate(VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE) )
        } else {
            @Suppress("DEPRECATION")
            vib.vibrate(1000)
        }
    }
}