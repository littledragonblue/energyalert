package eu.samoreira.energyalert

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.telephony.SmsManager
import android.util.Log
import eu.samoreira.energyalert.database.DrawModelClass
import eu.samoreira.energyalert.utils.Constants

class SmsService: Service() {
    private val TAG = "TAG SmsService"

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    private fun sendSMS(context: Context, phoneNumber: String, message: String) {
        val smsMessage = context.getString(R.string.app_name) + ": " + message

        try {
            // on below line we are initializing sms manager.
            //as after android 10 the getDefault function no longer works
            //so we have to check that if our android version is greater
            //than or equal toandroid version 6.0 i.e SDK 23
            val smsManager: SmsManager = if (Build.VERSION.SDK_INT >= 25) {
                //if SDK is greater that or equal to 23 then
                //this is how we will initialize the SmsManager
                context.getSystemService(SmsManager::class.java)
            } else{
                //if user's SDK is less than 25 then
                //SmsManager will be initialized like this
                SmsManager.getDefault()
            }

            // on below line we are sending text message.
            smsManager.sendTextMessage(phoneNumber, null, smsMessage, null, null)

            // on below line we are displaying a toast message for message send,
            //Toast.makeText(applicationContext, "Message Sent", Toast.LENGTH_LONG).show()
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "SMS Sent: $phoneNumber")
            }
        } catch (e: Exception) {
            // on catch block we are displaying toast message for error.
            EmailErrorReportService().sendEmail(context, e.message.toString(), "SmsService.sendSMS()")
            if (BuildConfig.DEBUG) {
                Log.d(TAG, e.message.toString())
            }
        }
    }

    fun sendAlertSms(context: Context, smsMessage: String, getPhones: ArrayList<DrawModelClass>) {
        if (!Constants.isPhoneActive) return

        for (phone in getPhones) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "${phone.contact} $smsMessage")
            }
            sendSMS(context, phone.contact, smsMessage)
        }
    }
}