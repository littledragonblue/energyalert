package eu.samoreira.energyalert.ui.home

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.Switch
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import eu.samoreira.energyalert.AddServer
import eu.samoreira.energyalert.utils.AppUtils
import eu.samoreira.energyalert.utils.Constants
import eu.samoreira.energyalert.database.DatabaseHandler
import eu.samoreira.energyalert.OpenInternetSettings
import eu.samoreira.energyalert.PermissionHelper
import eu.samoreira.energyalert.R
import eu.samoreira.energyalert.databinding.FragmentHomeBinding
import java.util.Calendar
import java.util.Locale

class HomeFragment: Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private var isBatteryServiceRegistered: Boolean = false
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var startTimeButton: Button
    private lateinit var endTimeButton: Button

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("UseSwitchCompatOrMaterialCode", "SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val root: View = binding.root

        val switchPhone: Switch = root.findViewById(R.id.switchPhone)
        val switchEmail: Switch = root.findViewById(R.id.switchEmail)
        val switchSom:   Switch = root.findViewById(R.id.switchSom)
        val delaySeekBar: SeekBar = root.findViewById(R.id.delaySeekBar)
        val textDelaySeekBar = root.findViewById<TextView>(R.id.textDelaySeekBar)
        startTimeButton = root.findViewById(R.id.startTimeButton)
        endTimeButton = root.findViewById(R.id.endTimeButton)

        if (Constants.isPhoneActive) {
            switchPhone.isChecked = true
        }

        if (Constants.isEmailActive) {
            switchEmail.isChecked = true
        }

        if (Constants.isSoundActive) {
            switchSom.isChecked = true
        }

        switchPhone.setOnClickListener { view ->
            val switch = view as Switch
            val databaseHandler = DatabaseHandler(requireContext())

            if (switch.isChecked) {
                if (!PermissionHelper.hasMultiplePermissions(
                        requireContext(), Manifest.permission.SEND_SMS)) {
                    switchPhone.isChecked = false
                    PermissionHelper.requestMultiplePermissions(
                        requireActivity(), Constants.SMS_PERMISSION_REQUEST_CODE, Manifest.permission.SEND_SMS)
                    return@setOnClickListener
                }

                when {
                    !isSimActive(requireContext()) -> {
                        switchPhone.isChecked = false
                        AppUtils.toast(context, getString(R.string.sim_not_active_message))
                    } isAirplaneModeOn(requireContext()) -> {
                        switchPhone.isChecked = false
                        AppUtils.toast(context, getString(R.string.airplane_mode_message))
                    } databaseHandler.viewPhones().isEmpty() -> {
                        switchPhone.isChecked = false
                        AppUtils.showSnackbar(root, getString(R.string.empty_phone_list))
                    } else -> {
                        Constants.isPhoneActive = true
                        AppUtils.toast(context, getString(R.string.phone_alert_activated))
                    }
                }
            } else {
                Constants.isPhoneActive = false
                AppUtils.toast(context, getString(R.string.phone_alert_desactivated))
            }

            databaseHandler.close()
        }

        switchEmail.setOnCheckedChangeListener { _, isChecked ->
            val databaseHandler = DatabaseHandler(requireContext())
            if (isChecked) {
                if (!AppUtils.getConnection(requireActivity())) {
                    switchEmail.isChecked = false
                    val openInternetSettings = OpenInternetSettings()
                    openInternetSettings.onClick(view)
                    AppUtils.showSnackbarNoInternet(view, getString(R.string.msg_internet_off))
                    return@setOnCheckedChangeListener
                }

                var shouldContinue = true
                AddServer.checkHttps(requireContext()) { useHttps ->
                    if (!useHttps) {
                        switchEmail.isChecked = false
                        showHttpsAlertDialog(requireContext())
                        shouldContinue = false
                    }

                    if (shouldContinue && databaseHandler.viewEmailSettings().isEmpty()) {
                        switchEmail.isChecked = false
                        AddServer.abrirEmail(requireContext())
                        shouldContinue = false
                    }

                    if (shouldContinue && databaseHandler.viewEmails().isEmpty()) {
                        switchEmail.isChecked = false
                        AppUtils.showSnackbar(root, getString(R.string.empty_email_list))
                        shouldContinue = true
                    }

                    if (shouldContinue) {
                        Constants.isEmailActive = true
                        AppUtils.toast(requireContext(), getString(R.string.email_alert_activated))
                    }
                }
            } else {
                Constants.isEmailActive = false
                AppUtils.toast(requireContext(), getString(R.string.email_alert_deactivated))
            }
            databaseHandler.close()
        }

        switchSom.setOnClickListener { view ->
            if ((view as Switch).isChecked) {
                if (PermissionHelper.checkAndRequestNotificationsPermissions(context as Activity)) {
                    Constants.isSoundActive = true
                    AppUtils.toast(context, getString(R.string.sound_alert_activated))
                } else {
                    switchSom.isChecked = false
                }
            } else {
                Constants.isSoundActive = false
                AppUtils.toast(context, getString(R.string.sound_alert_desactivated))
            }
        }

        val defaultAlarmeDelayTime = AppUtils.getSharedPreferencesInt(requireContext().applicationContext, "alarmDelayTime", 0)
        textDelaySeekBar.text = getString(R.string.alarm_delay_if_energy_off) + ": " + defaultAlarmeDelayTime
        delaySeekBar.progress = defaultAlarmeDelayTime

        val infoIcon = root.findViewById<ImageView>(R.id.infoIconDelay)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            infoIcon.tooltipText = getString(R.string.txt_tooltip_delay_alarm_off)
        } else {
            infoIcon.setOnLongClickListener {
                AppUtils.toast(requireContext(), getString(R.string.txt_tooltip_delay_alarm_off))
                true
            }
        }

        delaySeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                textDelaySeekBar.text = getString(R.string.alarm_delay_if_energy_off) + ": " + progress.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                val selectedValue = seekBar?.progress
                AppUtils.setSharedPreferencesInt(requireContext().applicationContext, "alarmDelayTime", selectedValue!!)
            }
        })

        val infoIconTime = root.findViewById<ImageView>(R.id.infoIconTime)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            infoIconTime.tooltipText = getString(R.string.txt_tooltip_time_range)
        } else {
            infoIconTime.setOnLongClickListener {
                AppUtils.toast(requireContext(), getString(R.string.txt_tooltip_time_range))
                true
            }
        }

        startTimeButton.setOnClickListener {
            showTimePickerDialog(requireContext(),true)
        }

        endTimeButton.setOnClickListener {
            showTimePickerDialog(requireContext(), false)
        }

        val savedStartTime = AppUtils.getSharedPreferencesString(requireContext(),"startTime", "00:00")
        savedStartTime?.let {
            startTimeButton.text = it
        }

        val savedEndTime = AppUtils.getSharedPreferencesString(requireContext(),"endTime", "00:00")
        savedEndTime?.let {
            endTimeButton.text = it
        }
    }

    private fun showTimePickerDialog(context: Context, isStartTime: Boolean) {
        val currentTime = Calendar.getInstance()
        val hour = currentTime.get(Calendar.HOUR_OF_DAY)
        val minute = currentTime.get(Calendar.MINUTE)

        val timePickerDialog = TimePickerDialog(context, { _, selectedHour, selectedMinute ->
            val timeFormatted = String.format(Locale.getDefault(), "%02d:%02d", selectedHour, selectedMinute)

            if (isStartTime) {
                startTimeButton.text = timeFormatted
                AppUtils.setSharedPreferencesString(context,"startTime", timeFormatted)
            } else {
                endTimeButton.text = timeFormatted
                AppUtils.setSharedPreferencesString(context,"endTime", timeFormatted)
            }
        }, hour, minute, true)

        timePickerDialog.show()
    }

    private fun isSimActive(context: Context): Boolean {
        val telephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        return telephonyManager.simState == TelephonyManager.SIM_STATE_READY
    }

    private fun isAirplaneModeOn(context: Context): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Settings.Global.getInt(context.contentResolver, Settings.Global.AIRPLANE_MODE_ON, 0) != 0
        } else {
            false
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.SMS_PERMISSION_REQUEST_CODE) {
            for (result in grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    AppUtils.toast(context, getString(R.string.sms_permission_not_granted_message))
                    return
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()

        if (isBatteryServiceRegistered) {
            isBatteryServiceRegistered = false
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun showHttpsAlertDialog(context: Context) {
        val alertDialog = AlertDialog.Builder(context)
            .setTitle(getString(R.string.secure_connection_error))
            .setMessage(getString(R.string.text_warning_secure_connection_error))
            .setPositiveButton(R.string.close) { dialog, _ ->
                dialog.dismiss()
            }
            .create()
        alertDialog.show()
    }
}