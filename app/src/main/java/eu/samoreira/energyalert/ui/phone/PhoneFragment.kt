package eu.samoreira.energyalert.ui.phone

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import eu.samoreira.energyalert.utils.AppUtils
import eu.samoreira.energyalert.BuildConfig
import eu.samoreira.energyalert.utils.Constants
import eu.samoreira.energyalert.database.DatabaseHandler
import eu.samoreira.energyalert.database.DrawModelClass
import eu.samoreira.energyalert.PhoneAdapter
import eu.samoreira.energyalert.R
import eu.samoreira.energyalert.databinding.FragmentPhoneListBinding

class PhoneFragment : Fragment() {

    private var _binding: FragmentPhoneListBinding? = null
    private val binding get() = _binding!!
    private lateinit var rvItemsList: RecyclerView
    private lateinit var tvNoRecordsAvailable: TextView

    private lateinit var phoneViewModel: PhoneViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        phoneViewModel = ViewModelProvider(this)[PhoneViewModel::class.java]

        _binding = FragmentPhoneListBinding.inflate(inflater, container, false)
        val root: View = binding.root
        val setName: EditText = root.findViewById(R.id.editTextName)
        val setContact: EditText = root.findViewById(R.id.contact)
        val btnAdd: Button = root.findViewById(R.id.btn_add)
        val btnCancel: Button = root.findViewById(R.id.btn_cancel)

        rvItemsList = root.findViewById(R.id.rvEmailList)
        tvNoRecordsAvailable = root.findViewById(R.id.tvNoRecordsAvailable)

        btnAdd.setOnClickListener {
            addContact(setContact, setName, rvItemsList, tvNoRecordsAvailable)
        }

        btnCancel.setOnClickListener {
            setName.text = null
            setContact.text = null
        }

        setupListofDataIntoRecyclerView(this.context as Activity, rvItemsList, tvNoRecordsAvailable)

        return root
    }

    private fun setupListofDataIntoRecyclerView(context: Activity, rvItemsList: RecyclerView, tvNoRecordsAvailable: TextView) {
        if (getItemsList(context as Context).size > 0) {
            rvItemsList.visibility = View.VISIBLE
            tvNoRecordsAvailable.visibility = View.GONE

            // Set the LayoutManager that this RecyclerView will use.
            rvItemsList.layoutManager = LinearLayoutManager(context)
            // Adapter class is initialized and list is passed in the param.
            val itemAdapter = PhoneAdapter(context, getItemsList(context as Context))
            // adapter instance is set to the recyclerview to inflate the items.
            rvItemsList.adapter = itemAdapter
        } else {
            rvItemsList.visibility = View.GONE
            tvNoRecordsAvailable.visibility = View.VISIBLE
        }
    }

    /**
     * Function is used to get the Items List from the database table.
     */
    private fun getItemsList(context: Context): ArrayList<DrawModelClass> {
        //creating the instance of DatabaseHandler class
        val databaseHandler = DatabaseHandler(context)
        return databaseHandler.viewPhones()
    }

    private fun String.toEditable(): Editable = Editable.Factory.getInstance().newEditable(this)

    private fun addContact(setContact: EditText, setName: EditText, rvItemsList: RecyclerView, tvNoRecordsAvailable: TextView) {
        val name = setName.text.toString()
        val getEmail = setContact.text.toString()
        val databaseHandler = DatabaseHandler(context as Activity)

        if (name.isNotEmpty()) {
            val status = databaseHandler.addPhone(DrawModelClass(0, getEmail, name))

            if (status > -1) {
                setName.text.clear()
                setContact.text.clear()
                setupListofDataIntoRecyclerView(this.context as Activity, rvItemsList, tvNoRecordsAvailable)
                AppUtils.toast(context as Activity, getString(R.string.contact_saved))
            }
        } else {
            AppUtils.toast(context as Activity, getString(R.string.contact_cannot_be_empty))
        }
    }

    fun updateContactDialog(drawModelClass: DrawModelClass, context: Activity) {
        val dialogUpdateContact = android.app.AlertDialog.Builder(context)
        dialogUpdateContact.setTitle(activity?.getString(R.string.update_contact))
        dialogUpdateContact.setCancelable(true)
        dialogUpdateContact.setIcon(R.drawable.ic_baseline_edit_24)

        val inflater = LayoutInflater.from(context)
        val dialogView: View = inflater.inflate(R.layout.layout_update_contact, null)
        dialogUpdateContact.setView(dialogView)

        val updateName = dialogView.findViewById<EditText>(R.id.editName)
        val updateContact = dialogView.findViewById<EditText>(R.id.editContact)
        val buttonAdd        = dialogView.findViewById<Button>(R.id.btn_update)
        val buttonCancel     = dialogView.findViewById<Button>(R.id.btn_cancel)
        rvItemsList          = context.findViewById(R.id.rvEmailList)
        tvNoRecordsAvailable = context.findViewById(R.id.tvNoRecordsAvailable)
        buttonAdd.text       = context.getString(R.string.update)

        val updateContactDialog = dialogUpdateContact.create()
        updateContactDialog.window!!.setSoftInputMode(1)
        updateName.text = drawModelClass.name.toEditable()
        updateContact.text = drawModelClass.contact.toEditable()
        updateContact.requestFocus()

        buttonAdd.setOnClickListener {
            val name = updateName.text.toString()
            val contact = updateContact.text.toString()
            val databaseHandler = DatabaseHandler(context)

            if (name.isNotEmpty() && contact.isNotEmpty()) {
                val status = databaseHandler.updatePhone(DrawModelClass(drawModelClass.id, contact, name))
                if (status > -1) {
                    setupListofDataIntoRecyclerView(context, rvItemsList, tvNoRecordsAvailable)
                    AppUtils.toast(context as Context, context.getString(R.string.contact_saved))
                    updateContactDialog.dismiss()
                }
            } else {
                AppUtils.toast(context as Context, context.getString(R.string.contact_cannot_be_empty))
            }

            updateContactDialog.dismiss()
        }
        buttonCancel.setOnClickListener {
            updateContactDialog.dismiss()
        }

        // Let's start with animation work. We just need to create a style and use it here as follows.
        if (updateContactDialog.window != null)
            updateContactDialog.window!!.attributes.windowAnimations = R.style.SlidingDialogAnimation
        updateContactDialog.show()
    }

    fun shareContact(drawModelClass: DrawModelClass, context: Activity) {
        try {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(
                Intent.EXTRA_TEXT,
                context.getString(R.string.app_name) + "\n" + drawModelClass.contact +
                        "\n\n\n" + context.getString(R.string.get_your_app) +
                        Constants.APP_DOWNLOAD_URL + BuildConfig.APPLICATION_ID
            )
            sendIntent.type = "text/plain"
            context.startActivity(sendIntent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun deleteContactAlertDialog(drawModelClass: DrawModelClass, context: Activity) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(context.getString(R.string.delete_contact))
        builder.setMessage(context.getString(R.string.request_delete_confirmation) + " ${drawModelClass.contact}.")
        builder.setIcon(R.drawable.ic_baseline_delete_24)
        rvItemsList          = context.findViewById(R.id.rvEmailList)
        tvNoRecordsAvailable = context.findViewById(R.id.tvNoRecordsAvailable)

        builder.setPositiveButton(context.getString(R.string.yes)) { dialogInterface, _ ->
            val databaseHandler = DatabaseHandler(context)
            val status = databaseHandler.deletePhone(DrawModelClass(drawModelClass.id, "", ""))

            if (status > -1) {
                AppUtils.toast(context as Context, context.getString(R.string.contact_deleted_success))
                setupListofDataIntoRecyclerView(context, rvItemsList, tvNoRecordsAvailable)
            }

            dialogInterface.dismiss()
        }

        builder.setNegativeButton(context.getString(R.string.no)) { dialogInterface, _ ->
            dialogInterface.dismiss()
        }

        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()

        val buttonbackground: Button = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE)
        buttonbackground.setTextColor(Color.rgb(0, 200, 0))

        val buttonbackground1: Button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE)
        buttonbackground1.setTextColor(Color.rgb(200, 0, 0))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}