package eu.samoreira.energyalert.ui.phone

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel

class PhoneViewModel : ViewModel() {

    val name = MutableLiveData<String>()
    private val phoneList = MutableLiveData<List<PhoneItem>>()
    private val hasRecords = MutableLiveData(false)

    private val phoneListObserver = Observer<List<PhoneItem>> { emails ->
        hasRecords.value = emails.isNotEmpty()
    }

    init {
        phoneList.observeForever(phoneListObserver)
    }

    override fun onCleared() {
        phoneList.removeObserver(phoneListObserver)
        super.onCleared()
    }
}

data class PhoneItem(val name: String, val email: String)