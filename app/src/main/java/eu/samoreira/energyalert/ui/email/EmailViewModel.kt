package eu.samoreira.energyalert.ui.email

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel

class EmailViewModel : ViewModel() {

    val name = MutableLiveData<String>()
    val email = MutableLiveData<String>()

    private val emailList = MutableLiveData<List<EmailItem>>()
    private val hasRecords = MutableLiveData(false)

    private val emailListObserver = Observer<List<EmailItem>> { emails ->
        hasRecords.value = emails.isNotEmpty()
    }

    init {
        emailList.observeForever(emailListObserver)
    }

    override fun onCleared() {
        emailList.removeObserver(emailListObserver)
        super.onCleared()
    }
}

data class EmailItem(val name: String, val email: String)