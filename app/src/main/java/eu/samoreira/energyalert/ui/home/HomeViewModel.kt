package eu.samoreira.energyalert.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    // LiveData to control SMS option
    private val _isSMSEnabled = MutableLiveData<Boolean>()
    val isSMSEnabled: LiveData<Boolean> = _isSMSEnabled

    // LiveData to control Email option
    private val _isEmailEnabled = MutableLiveData<Boolean>()
    val isEmailEnabled: LiveData<Boolean> = _isEmailEnabled

    // LiveData to control Sound option
    private val _isSoundEnabled = MutableLiveData<Boolean>()
    val isSoundEnabled: LiveData<Boolean> = _isSoundEnabled

    // LiveData to show TextView info
    private val _infoText = MutableLiveData<String>()
    val infoText: LiveData<String> = _infoText

    init {
        _isSMSEnabled.value = false
        _isEmailEnabled.value = false
        _isSoundEnabled.value = false
        _infoText.value = "Start Info"
    }
}