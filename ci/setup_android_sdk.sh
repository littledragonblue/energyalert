#!/bin/bash

apt-get --quiet update --yes
apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1

# Definir ANDROID_HOME
export ANDROID_HOME="${PWD}/android-home"
echo "ANDROID_HOME set to $ANDROID_HOME"

# Criar diretório para o SDK do Android
install -d $ANDROID_HOME

# Baixar e descompactar as ferramentas de linha de comando do Android SDK
wget --output-document=$ANDROID_HOME/cmdline-tools.zip "https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip"
pushd $ANDROID_HOME
unzip -d cmdline-tools cmdline-tools.zip
pushd cmdline-tools
mv cmdline-tools tools || true
popd
popd
export PATH=$PATH:${ANDROID_HOME}/cmdline-tools/tools/bin/

# Instalar componentes do SDK
yes | sdkmanager --licenses || true
sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}"
sdkmanager "platform-tools"
sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}"

chmod +x ./gradlew
