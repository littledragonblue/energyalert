# Energy Alert ⚡️

![Energy Alert Logo](https://gitlab.com/littledragonblue/energyalert/-/raw/main/Extras/icon_energyalert_114x114.png) 

## About 📖

Ever been caught off guard by unexpected power outages? I faced the same challenge in my condo's garage. Traditional market solutions came with hefty price tags and limited functions. Leveraging an old Android phone, I birthed a more flexible, cost-effective solution: Energy Alert.

## Features ✨

- **Instant Alerts**: Get immediate notifications in case of a power outage.
- **Multiple Notification Methods**: Choose from sound alerts, SMS, or email.
- **Configurable Monitoring Window**: Define the time window for monitoring.
- **Delay Alerts**: Customize the delay for alerts in case of short-term power disruptions.

## How It Works 🛠

1. **Install**: [Download "Energy Alert"](https://samoreira.eu/?download=energyalert) and install it on an Android device.
2. **Configure**: Set up the alert types (sound, SMS, email) you want to receive.
3. **Monitor**: Place the device in a location where it can monitor the power supply.
4. **Stay Informed**: Get alerts when there's a disruption.

## Contributing 🤝

We welcome contributions to "Energy Alert"! If you have a feature request, bug report, or want to improve the app, please:

- Fork this repository.
- Create a new feature branch.
- Make your changes and submit a pull request.

## License ⚖️

"Energy Alert" is open-source and is licensed under the MIT License.

## Credits ❤️

A special thanks to the community for their continuous support and contributions. This app wouldn't have been possible without you!
